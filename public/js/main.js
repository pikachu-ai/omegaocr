$(document).ready(function() { 
    $(".convert-button button").click(function() { 
        link = $(".convert-button button").prop('donwload_link')
        if(link){
            window.open(link)
        }
        
    });

    $(".dt-file-name-inner").click(function() { 
        link = $(".dt-file-name-inner").prop('debug')
        if(link){
            window.open(link)
        }
        
    });

    $("#pc-upload-add").change(function() {
        if (!this.files[0]) {
            return
        }
        fd = new FormData(); 
        fd.append('file', this.files[0])
        console.log(this.files[0].name);
        filename = this.files[0].name
        $("#loading_container").show()
        $.ajax({ 
            url: '/', 
            type: 'post', 
            data: fd, 
            contentType: false, 
            processData: false, 
            success: function(response){ 
                if(response != 0){ 
                   
                    $(".files-container").show()
                    $(".converter-wrapper").hide()

                    $(".dt-file-name-inner span").text(filename)
                    $("#loading_container").hide()
                    console.log(response);
                    $(".convert-button button").prop('disabled', false);
                    $(".convert-button button").prop('donwload_link', response['link']);
                    $(".dt-file-name-inner").prop('debug', response['debug']);
                } 
                else{ 
                    alert('file not uploaded'); 
                    $("#loading_container").hide()
                    $(".convert-button button").prop('disabled', true);
                } 
            }, 
        }); 
    });
}); 