﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using reconstruct.Models;
using Reconstructor;
using System.Web.Http.Cors;

namespace reconstruct.Controllers
{
    [EnableCors(origins: "http://localhost:8000", headers: "*", methods: "*")]
    public class ProductsController : ApiController
    {

        [HttpPost]
        public IHttpActionResult create(Product json_string)
        {
            System.Diagnostics.Debug.WriteLine(json_string);

            bool status = DocumentReconstructor.CreateDocument(json_string, "output.docx");
            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");

            return Ok(json_string);
        }
    }
}
