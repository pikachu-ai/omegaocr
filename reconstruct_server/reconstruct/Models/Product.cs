﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Reconstructor;

namespace reconstruct.Models
{
    public class Product
    {
        public int width { get; set; }
        public int height { get; set; }
        public ObjData[] datalist { get; set; }
    }
}