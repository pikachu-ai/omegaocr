import os
import sys

sys.path.append('app')
import cv2
import argparse
from glob import glob
from time import gmtime, strftime, time
from layout.layout import RCNNLayoutAnalysis
from extractor.doc_extractor import DocExtractor
from controller.debuger import draw_debug



if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--image_dir', "--verbose", help="input image folder", type=str, default='image_dir')
    parser.add_argument('--debug_dir', help="input image folder", type=str, default=None)
    args = parser.parse_args()
    image_dir_path = args.image_dir
    debugs = args.debug_dir

    if debugs:
        dbg_name = strftime("%Y-%m-%d-%H-%M-%S", gmtime())
        # debugs = os.path.join(debugs, dbg_name)
        os.makedirs(debugs, exist_ok=True)

    image_list = glob(os.path.join(image_dir_path, '*.jpg'))
    extractor = DocExtractor()
    layout_extractor = RCNNLayoutAnalysis()
    for image_path in image_list[:]:
        print(f"----------------------------- {image_path} -----------------------")
        img_name = os.path.basename(image_path).split('.')[0]
        img_debug = None
        if debugs:
            img_debug = os.path.join(debugs, img_name)
            os.makedirs(img_debug, exist_ok=True)

        t = time()
        image = cv2.imread(image_path)

        image = extractor.get_doc_image(image, img_debug)
        layout = layout_extractor.get_layout(image)

        if img_debug:
            draw_debug(image.copy(), layout, os.path.join(img_debug, 'image.jpg'))
        print(time()-t)


