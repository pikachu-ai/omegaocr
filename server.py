import os
from flask import Flask, flash, request, redirect, url_for, send_from_directory
from flask_cors import CORS
from werkzeug.utils import secure_filename
import cv2
import sys
import json
sys.path.append('app')
sys.path.append('app/layout')

# from app.controller.controller import VanillaController
# from extractor.doc_extractor import DocExtractor
# from layout.layout import RCNNLayoutAnalysis
# from ocr.ocr import Seq2SeqOCR


# ocr_engine = CRnnOCR()
# ocr_engine = Seq2SeqOCR()
# reconstructor = DocxReconstructor()
# extractor = DocExtractor()
# layout_engine = RCNNLayoutAnalysis()


# detector = VanillaController(
#     layout_engine=layout_engine,
#     ocr_engine=ocr_engine,
#     reconstructor=reconstructor,
#     extractor=extractor
# )

UPLOAD_FOLDER = 'public\\inputs'
SAVE_FOLDER = 'public\\outputs'
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}

app = Flask(
    __name__,
    static_url_path='', 
    # static_folder='public',
    template_folder='public'
)
CORS(app)

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['SAVE_FOLDER'] = SAVE_FOLDER


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/', defaults=dict(foldername=None, filename=None))
@app.route('/<path:foldername>/<path:filename>')
def index(foldername, filename):
    if foldername is None or filename is None:
        filename = 'index.html'
        if request.method == 'GET':
            return send_from_directory('public', filename)

    file_path = os.path.join('public', foldername, filename)
    print(file_path)
    if os.path.isfile(file_path):
        return send_from_directory(os.path.dirname(file_path), filename)


@app.route('/', methods=['POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename

        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)

        print("file and allowed_file(file.filename): ", file, allowed_file(file.filename), request.url)
        if file and allowed_file(file.filename):
            
            filename = secure_filename(file.filename)
            file_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(file_path)
            output_path = os.path.join(app.config['SAVE_FOLDER'], filename+'.docx')
            image = cv2.imread(file_path)
            # data = detector.gen_docx(image, output_path)

            # the .docx file is saved input outputs/ before by reconstruct API
            out_link = f"{request.url}outputs/{filename+'.docx'}"
            # out_link = f"{request.url}outputs/{'output' + '.docx'}"
            # data["debug"] = f"{request.url}debug/{filename}"
            # return {"data": json.dumps(data)}
            # return json.dumps(data)
            return {
                "link": out_link,
                "debug": f"{request.url}debug/{filename}"
            }


    return {"message": "not allowed method"}


if __name__ == '__main__':
    app.run(host="localhost", debug=True)