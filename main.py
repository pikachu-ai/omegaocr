import cv2
import numpy as np
import json


def to_points(dic):
    return np.array([list(pt.values()) for pt in dic])


def scale_points(pts: np.array, ratio=1.0):
    return pts*ratio


def resize_image(image: np.array):
    return cv2.resize(image, )


def draw_box(image, pts):
    isClosed = True

    # Blue color in BGR
    color = (255, 0, 0)

    # Line thickness of 2 px
    thickness = 2

    # Using cv2.polylines() method
    # Draw a Blue polygon with
    # thickness of 1 px
    image = cv2.polylines(image, [pts],
                          isClosed, color, thickness)
    return image

#
# def draw_para(anotations):
#     for para in anotations.responses.fullTextAnnotation.pages.blocks.


if __name__ == "__main__":
    # path
    path = "data/google_img/gg_img105.jpg"
    anotations = json.load(open('gg_api.json', 'r', encoding='utf8'))
    # Reading an image in default
    # mode
    image = cv2.imread(path)

    # Window name in which image is
    # displayed
    window_name = 'Image'
    dic = [
        {
                      "x": 492,
                      "y": 239
                    },
                    {
                      "x": 669,
                      "y": 239
                    },
                    {
                      "x": 669,
                      "y": 254
                    },
                    {
                      "x": 492,
                      "y": 254
                    }
                  ]

    pts = to_points(dic)

    image = draw_box(image, pts)

    # Displaying the image

    cv2.imshow('image', image)
    cv2.waitKey()

    cv2.destroyAllWindows()
