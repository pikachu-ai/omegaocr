import scrapy
import urllib.parse as urlparse
from scrapy.http import Request
import os


class HousePrices(scrapy.Spider):
    name = 'bds'
    start_urls = [
        'https://www.moit.gov.vn/web/guest/van-ban-dieu-hanh?p_auth=RuqNowAW&p_p_id=ELegalDocumentView_WAR_ELegalDocumentportlet_INSTANCE_bxuLX3gTZKHk&p_p_lifecycle=1&p_p_state=normal&p_p_mode=view&p_p_col_id=column-1&p_p_col_count=1&_ELegalDocumentView_WAR_ELegalDocumentportlet_INSTANCE_bxuLX3gTZKHk_javax.portlet.action=executeSearch',
        'https://www.moit.gov.vn/web/guest/van-ban-dieu-hanh?p_auth=RuqNowAW&p_p_id=ELegalDocumentView_WAR_ELegalDocumentportlet_INSTANCE_bxuLX3gTZKHk&p_p_lifecycle=1&p_p_state=normal&p_p_mode=view&p_p_col_id=column-1&p_p_col_count=1&_ELegalDocumentView_WAR_ELegalDocumentportlet_INSTANCE_bxuLX3gTZKHk_javax.portlet.action=executeSearch',
        'https://www.moit.gov.vn/web/guest/van-ban-dieu-hanh?p_auth=RuqNowAW&p_p_id=ELegalDocumentView_WAR_ELegalDocumentportlet_INSTANCE_bxuLX3gTZKHk&p_p_lifecycle=1&p_p_state=normal&p_p_mode=view&p_p_col_id=column-1&p_p_col_count=1&_ELegalDocumentView_WAR_ELegalDocumentportlet_INSTANCE_bxuLX3gTZKHk_javax.portlet.action=executeSearch'
    ]

    def parse(self, response):
        # print(response.css('tr td a'))
        for quote in response.css('tr td a'):
            main_link = quote.css('::attr("href")').get()
            # print(main_link)
            if 'document_id' in main_link:
                yield response.follow(main_link, self.parse_main_page)

    
    def parse_main_page(self, response):
        
        for e in response.css('a.doc_detail_file_link'):
            pdf_link = e.css('::attr("href")').get()
            # print(pdf_link)
            yield Request(
                url=response.urljoin(pdf_link),
                callback=self.save_pdf
            )
            # break
    
    def save_pdf(self, response):
        path = os.path.join('datas', response.url.split('/')[-1])
        # self.logger.info('Saving PDF %s', path)
        with open(path, 'wb') as f:
            f.write(response.body)


            