import scrapy
import urllib.parse as urlparse
from scrapy.http import Request
import os


class HousePrices(scrapy.Spider):
    name = 'bds'
    start_urls = [
        'http://vanban.chinhphu.vn/portal/page/portal/chinhphu/hethongvanban?class_id=1&mode=view&org_group_id=0&type_group_id=4&category_id=0',
    ]

    def parse(self, response):
        # print(response.css('tr td a'))
        for quote in response.css('tr td a'):
            main_link = quote.css('::attr("href")').get()
            # print(main_link)
            if 'document_id' in main_link:
                yield response.follow(main_link, self.parse_main_page)

    
    def parse_main_page(self, response):
        
        for e in response.css('a.doc_detail_file_link'):
            pdf_link = e.css('::attr("href")').get()
            # print(pdf_link)
            yield Request(
                url=response.urljoin(pdf_link),
                callback=self.save_pdf
            )
            # break
    
    def save_pdf(self, response):
        path = os.path.join('datas', response.url.split('/')[-1])
        # self.logger.info('Saving PDF %s', path)
        with open(path, 'wb') as f:
            f.write(response.body)


            