import scrapy
import urllib.parse as urlparse
from scrapy.http import Request
import os


# http://vbpl.vn/botuphap/Pages/Home.aspx (pdf gốc + searchable image pdf)
# https://emohbackup.moh.gov.vn/publish/home


class Crawler(scrapy.Spider):
    name = 'bds'
    start_urls = [
        'http://vanban.chinhphu.vn/portal/page/portal/chinhphu/hethongvanban?class_id=1&mode=view&org_group_id=0&type_group_id=6&category_id=0',
        # 'https://emohbackup.moh.gov.vn/publish/home'
        # 'http://www.xaydung.gov.vn/vn/Pages/ChiTietVanBan.aspx?vID=3745&TypeVB=1'
    ]

    def parse(self, response, **kwargs):
        # print(response.css('tr td a'))
        count = 0
        for quote in response.css('tr td a'):
            main_link = quote.css('::attr("href")').get()
            # print(main_link)

            if 'document_id' in main_link:
                count += 1
                print(count)
                yield response.follow(main_link, self.parse_main_page)

    def parse_main_page(self, response):
        # c = 0
        for e in response.css('a.doc_detail_file_link'):
            pdf_link = e.css('::attr("href")').get()
            # print(pdf_link)
            # c += 1
            # print(count+c)
            yield Request(
                url=response.urljoin(pdf_link),
                callback=self.save_pdf
            )
            # break
    
    def save_pdf(self, response):
        path = os.path.join('data/vanphong.chinhphu.vn/thong_tu-hanh_chinh', response.url.split('/')[-1])
        # self.logger.info('Saving PDF %s', path)
        with open(path, 'wb') as f:
            f.write(response.body)
