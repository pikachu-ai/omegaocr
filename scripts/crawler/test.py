import numpy as np
import cv2

image = cv2.imread('C:/Users/tupm3/Downloads/HT_dinhhuonggiaiphapAEC 001.jpg')
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
ret, thresh = cv2.threshold(gray, 200, 255, cv2.THRESH_BINARY_INV)
# print(thresh.shape)
kernel = np.ones((3,3),np.uint8)
dilated = cv2.dilate(thresh.T, kernel, iterations=5)
cv2.imshow('image', image)
cv2.imshow('dilated', dilated.T)
cv2.waitKey(0)