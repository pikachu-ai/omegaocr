import os
from os import listdir
from os.path import isfile, join

path = 'images' # path to image folder

listfile = listdir(path)
for f in listfile:
    if f[-4:] == '.jpg':
        name = f.replace('.jpg', '')
        if name+".xml" not in listfile:
            os.remove(join(path, f))