import os

image_list = os.listdir('../cameraImg')

for idx, img_name in enumerate(image_list):
    os.rename(f'../cameraImg/{img_name}', f'../cameraImg/{idx}.jpg')
