# loop through extract_shot_layout to take transformed page images.
# crop line images based detection result and data.json

import sys
import os
import time
from shutil import copyfile
import numpy as np
import json
import cv2
from vietocr.train import Trainer
import ntpath


def get_file_path_by_type(path, extensions=('.jpg', '.png')):
    img_paths = []
    for x in os.listdir(path):
        for ext in extensions:
            if x.endswith(ext):
                img_paths.append(path + '/' + x)

    return img_paths


def drop_extension(file_path):
    """
    Drop file extension of a file path.
    """
    return os.path.splitext(file_path)[0]


def collect(path, src_file_name, dst_folder):
    if not os.path.exists(dst_folder):
        os.mkdir(dst_folder)
        
    sub_folders = os.listdir(path)
    for img_folder in sub_folders:
        copyfile(path + "/" + img_folder + "/" + src_file_name, 
                    dst_folder + "/" + img_folder + "_" + src_file_name)


def crop_rectangle(img, box=(None, None, None, None)):
    x1, y1, x2, y2 = box
    return img[y1:y2, x1:x2, :]


def crop_lines(src_folder, dst_folder):
    if not os.path.exists(dst_folder):
        os.mkdir(dst_folder)

    sub_folders = os.listdir(src_folder)
    for img_folder in sub_folders:
        json_path = src_folder + "/" + img_folder + "/" + 'data.json'
        img_path = src_folder + "/" + img_folder + "/" + 'adaptive.jpg'
        layout = json.load(open(json_path, 'r', encoding='utf8'))
        src_img = cv2.imread(img_path)

        for idx, component in enumerate(layout):
            x1, y1, x2, y2 = component['location'].values()
            if x2-x1 < 32:
                x2 = x1 + y2-y1 + 3
            cut_img = crop_rectangle(src_img, box=(x1, y1-3, x2, y2))

            # save lines
            line_img_path = dst_folder + "/" + img_folder + "_" + str(idx) + ".jpg"
            cv2.imwrite(line_img_path, cut_img)


def rename(folder_path, match='adaptive', replace_with='document', extensions=('.json',)):
    file_paths = get_file_path_by_type(folder_path, extensions=extensions)
    for path in file_paths:
        new_path = path.replace(match, replace_with)
        os.rename(path, new_path)
        print(f"Old path: {path}, new_path: {new_path}")


def path_leaf(path):
    head, tail = ntpath.split(path)
    return tail or ntpath.basename(head)
    

def rename_to_index(folder_path, key_extensions=('.jpg',), start_index=7000):
    file_paths = get_file_path_by_type(folder_path, extensions=key_extensions)
    for idx, old_img_path in enumerate(file_paths):
        old_prefix = drop_extension(old_img_path)
        new_prefix = folder_path + '/' + str(idx + start_index)
        new_img_path = new_prefix + '.jpg'
        old_json_line_path = old_prefix + '_lines.json'
        new_json_line_path = new_prefix + '_lines.json'
        old_json_response_path = old_prefix + '_response.json'
        new_json_response_path = new_prefix + '_response.json'
        os.rename(old_img_path, new_img_path)
        os.rename(old_json_line_path, new_json_line_path)
        os.rename(old_json_response_path, new_json_response_path)
        print(f"Old path: {old_img_path}, new_path: {new_img_path}")
        

if __name__ == "__main__":
    # collect("extracted_debug_items", 
    #         src_file_name='document.jpg', 
    #         dst_folder="extracted_shot_img")

    # collect(path="extracted_debug_items", 
    #         src_file_name='data.json', 
    #         dst_folder="extracted_shot_img")

    crop_lines(src_folder='extracted_shot_img', dst_folder='line_imgs')
    # rename('extracted_shot_img', match='adaptive', replace_with='document', extensions=('.json',))
    # rename_to_index('extracted_shot_img', key_extensions=('.jpg',), start_index=7000)