import sys
import os
import time
from shutil import copyfile
import numpy as np
import json
import cv2


def get_file_path_by_type(path, extensions=('.jpg', '.png')):
    img_paths = []
    for x in os.listdir(path):
        for ext in extensions:
            if x.endswith(ext):
                img_paths.append(path + '/' + x)

    return img_paths


def drop_extension(file_path):
    """
    Drop file extension of a file path.
    """
    return os.path.splitext(file_path)[0]


def collect(path, src_file_name, dst_folder):
    if not os.path.exists(dst_folder):
        os.mkdir(dst_folder)
        
    sub_folders = os.listdir(path)
    for img_folder in sub_folders:
        copyfile(path + "/" + img_folder + "/" + src_file_name, 
                    dst_folder + "/" + img_folder + "_" + src_file_name)

def combine_text_file(src, dst):
    """
    Combine text files into one .txt file.
    """



def build_data_ocr():
    pass


if __name__ == "__main__":
    # collect("extracted_debug_items", 
    #         src_file_name='document.jpg', 
    #         dst_folder="extracted_shot_img")

    # collect(path="extracted_debug_items", 
    #         src_file_name='data.json', 
            # dst_folder="extracted_shot_img")

    crop_lines(src_folder='extracted_debug_items', dst_folder='line_imgs')