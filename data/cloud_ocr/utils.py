import os
import sys
import json
import time
import requests
# If you are using a Jupyter notebook, uncomment the following line.
# %matplotlib inline
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from PIL import Image
from io import BytesIO
import cv2


def get_lines_bounding(response_json):
    line_boxes = []
    lines = [{'boundingBox': line['boundingBox'], 'words': line['words']} for region in response_json["regions"] for
             line in region["lines"]]
    #     return lines
    for line in lines:
        bbox = [int(num) for num in line['boundingBox'].split(",")]
        text = ' '.join([word['text'] for word in line['words']])
        line_boxes.append({'boundingBox': bbox, 'text': text})

    return line_boxes


def save_json(obj, full_file_path):
    with open(full_file_path, 'w', encoding='utf8') as file:
        json.dump(obj, file, indent=2, ensure_ascii=False)


def drop_extension(file_path):
    """
    Drop file extension of a file path.
    """
    return os.path.splitext(file_path)[0]


def get_file_path_by_type(path, extensions=('.jpg', '.png')):
    img_paths = []
    for x in os.listdir(path):
        for ext in extensions:
            if x.endswith(ext):
                img_paths.append(path + '/' + x)

    return img_paths


def get_all_files(path):
    file_paths = []
    for x in os.listdir(path):
        file_paths.append(path + '/' + x)

    return file_paths


class AzureOCR:
    subscription_key = "0dea9d588d4e464086a0c0f89fa747a2"
    endpoint = "https://xuanhai.cognitiveservices.azure.com/"

    # ocr_api_url = endpoint + "vision/v3.1/ocr"
    ocr_api_url = endpoint + "/vision/v3.1/read/analyze"

    # Set Content-Type to octet-stream
    headers = {'Ocp-Apim-Subscription-Key': subscription_key, 'Content-Type': 'application/octet-stream'}
    params = {'language': 'unk',
              #           'detectOrientation': 'true'
              }

    @classmethod
    def request_azure_ocr(cls, image_data):
        response = cloud_ocr.post(cls.ocr_api_url, headers=cls.headers,
                                  params=cls.params,
                                  data=image_data)
        response.raise_for_status()

        analysis = response.json()

        return analysis

    @classmethod
    def request_azure_read_api(cls, image_data):
        text_recognition_url = cls.endpoint + "/vision/v3.1/read/analyze"

        headers = {'Ocp-Apim-Subscription-Key': cls.subscription_key,
                   'Content-Type': 'application/octet-stream'
                   }

        response = cloud_ocr.post(
            text_recognition_url, headers=headers,
            #     json=data,
            data=image_data
        )

        response.raise_for_status()

        # Extracting text requires two API calls: One call to submit the
        # image for processing, the other to retrieve the text found in the image.

        # Holds the URI used to retrieve the recognized text.
        operation_url = response.headers["Operation-Location"]

        # The recognized text isn't immediately available, so poll to wait for completion.
        analysis = {}
        poll = True
        while poll:
            response_final = cloud_ocr.get(
                response.headers["Operation-Location"], headers=headers)
            analysis = response_final.json()

            #     print(json.dumps(analysis, indent=4))

            time.sleep(0.01)  # wait for completion
            if "analyzeResult" in analysis:
                poll = False
            if "status" in analysis and analysis['status'] == 'failed':
                poll = False

        return analysis

    @staticmethod
    def get_line_polygons_read_api(analysis):
        polygons = []
        if "analyzeResult" in analysis:
            # Extract the recognized text, with bounding boxes.
            polygons = [{"boundingBox": line["boundingBox"], "text": line["text"]}
                        for line in analysis["analyzeResult"]["readResults"][0]["lines"]]

        return polygons
