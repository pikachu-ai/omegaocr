# OmegaOCR 
**End-to-end OCR API**

<b>Take the image of business document and convert to searchable digital document.</b> <br>

- All the data, both crawled raw pdfs and processed images can be downloaded <a href="https://drive.google.com/drive/folders/15bhDqXXLca1N9Bg-12EhgIC2hFjajAuW?usp=sharing">here</a>.
- Our Document Object Detection <a href="https://drive.google.com/drive/folders/1SsUFEkNbQ80MuGkMMkLSATcSIv-qu7qT?usp=sharing">Model</a>
- Our OCR <a href="https://drive.google.com/drive/folders/1vLTDVh7tFnF3rv6g73JYQw2nHYSoeZ3y?usp=sharing">Model</a>

## Phase 1: First running system
- Xây dựng pipeline hệ thống nhận ảnh scan <a href="images/sample_input.jpg">văn bản hành chính</a> (hợp đồng, công văn, thông tư, thông báo...) và chuyển thành tài liệu docx.
- Tập trung phát triển mô hình Document Object Detection, phát hiện chính xác text line.
- WebApp demo, phát triển phần mềm theo mô hình MVC, deploy bằng Flask API.

## Phase 2: Finetune and improve, add more features
- Cải thiện mô hình OCR: tăng số lượng data, fine tune model Seq2seq.
- Document Object Detection: Train lại với dữ liệu mới từ ảnh chụp điện thoại.
- Layout Analysis: block -> paragraph -> line inference.
- Text Correction: Sửa lỗi chính tả cho module OCR.
- Reconstruction: sử dụng Word API trên C# thay cho python-docx
- Hoàn thiện API, WebApp: nhận file ảnh chụp từ điện thoại và trả về tài liệu.

### Main Flow
<img src='images/main_flow.png'>

### Structure (UML Class Diagram)
Detail Structure can be found in our report.  
<img src='images/structure.jpeg'>

### Demo User Interface 
<img src='images/ui.png'>
