#finding hsv range of target object(pen)
import cv2
import numpy as np
import time
import imutils
from glob import glob
# A required callback method that goes into the trackbar function.

image_list = glob("/home/tupm/SSD/Courses/DL/Exercises/omegaocr/extract_layout_debugs/*/document.jpg")

def test(img_path):
    image = cv2.imread(img_path)

    image = imutils.resize(image, 500)

    adaptive = cv2.adaptiveThreshold(cv2.cvtColor(image, cv2.COLOR_BGR2GRAY), 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)
    # adaptive = cv2.medianBlur(adaptive, 3)

    num_labels, labels, stats, centroids = cv2.connectedComponentsWithStats(255 - adaptive , 8 , cv2.CV_32S)

    mask = np.zeros(adaptive.shape, dtype=np.uint8)

    for idx, stat in enumerate(stats):
        x, y, w, h, c = stat

        if w > 50 and h > 50 and h < 500:
            print(idx)
            mask[labels==idx] = 255

    image[mask==255] = np.array([0, 0, 255])
    cv2.imshow("image", image)
    cv2.imshow("adaptive", mask)
    cv2.waitKey(0)

for img in image_list:
    test(img)