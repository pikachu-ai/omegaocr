from interfaces.layout_interfaces import AbstractLayout
import os
import numpy as np
from glob import glob
import json


class RuleBasedLayout(AbstractLayout):
    """
    Layout Analysis using Rulebased method.
    """
    @staticmethod
    def get_texts(doc):
        """
        Đầu vào:
            - doc: list của các dict, chứa thông tin của một văn bản. Mỗi dict là
                một dòng trong văn bản gồm 2 thông tin là boungbox và text.

        Đầu ra:
            - doc_text: list của các strings, mỗi string là thông tin 'text' của một
                dòng chữ trong văn bản doc.
        """
        doc_text = []
        for sent in doc:
            doc_text.append(sent['text'])
        return doc_text

    @staticmethod
    def get_vecs(doc):
        """
        Đầu vào:
            - doc: list của các dict, giống như trong hàm get_texts.

        Đầu ra:
            - coordinates: list của các tuple. Mỗi tuple là một bộ 8 số biểu diễn bounding box của một dòng
                text trong văn bản, tức là (x1, y1, x2, y2, x3, y3, x4, y4). (x1, y1) là góc trên bên trái, các
                góc còn lại tính theo chiều kim đồng hồ.
            - feature_vecs: list của các tuple. Mỗi tuple là một bộ 4 số biểu diễn trung điểm cạnh trái và phải
                của bounding box của dòng text, tức là (x1', y1', x2', y2'), trong đó x1' = (x1 + x4)/2
                    y1' = (y1 + y4)/2, x2' = (x2 + x3)/2, y2' = (y2 + y3)/2
            - feature_vecs2: list của các tuple. Mỗi tuple là một bộ 2 số biểu diễn trung điểm cạnh trái
                của bounding box, tức là chỉ lấy (x1', y1') - công thức như trên.
        """
        coordinates = []
        for sent in doc:
            coordinates.append(sent['boundingBox'])

        feature_vecs = []
        feature_vecs2 = []
        for coordinate in coordinates:
            feature_vec = [(coordinate[0] + coordinate[6]) / 2,
                           (coordinate[1] + coordinate[7]) / 2,
                           (coordinate[2] + coordinate[4]) / 2,
                           (coordinate[3] + coordinate[5]) / 2]
            feature_vecs.append(feature_vec)
            vec = [(coordinate[0] + coordinate[6]) / 2, (coordinate[1] + coordinate[7]) / 2]
            feature_vecs2.append(vec)

        return coordinates, feature_vecs, feature_vecs2

    @staticmethod
    def distance_threshold(feature_vecs2, doc_len, quantile=0.05):
        '''
        Đầu vào:
            - feature_vecs2: list của các tuple. Đầu ra của hàm get_vecs
            - doc_len: độ dài của doc (doc là một list gồm các dict).
            - quantile: số trong khoảng (0, 1). Thể hiện độ hào phóng trong việc tạo block.
                + quantile càng gần 1: càng hào phóng: các dòng cách nhau xa vẫn có thể
                    về chung 1 block
                + quantile càng gần 0: càng ngặt nghèo: các dòng chỉ cần cách nhau xa một
                    chút sẽ lập tức về các block khác nhau.
        Đầu ra:
            - dists: ma trận numpy có kích thước (doc_len + 1) x (doc_len + 1). Hàng đầu tiên
                và cột đầu tiên lưu lại chỉ số của các dòng trong doc, các ô còn lại trong ma trận
                lưu lại bình phương khoảng cách đôi một giữa các dòng, tính theo lề trái.

                dists là ma trận đối xứng, do đó quy ước chỉ điền phần nửa tam giác trên. Đường chéo
                chính và phần nửa tam giác dưới cho bằng 0.
            - thres: khoảng cách phân chia, các dòng text có khoảng cách lớn hơn thres sẽ thuộc 2 block
                khác nhau.
        '''
        dists = np.zeros((doc_len + 1, doc_len + 1))

        dists[0, 0] = -1
        dists[1:doc_len + 1, 0] = np.arange(doc_len)
        dists[0, 1:doc_len + 1] = np.arange(doc_len)

        dist_list = []
        for i in range(doc_len - 1):
            vec1 = feature_vecs2[i]
            for j in range(i + 1, doc_len):
                vec2 = feature_vecs2[j]
                dist = (vec2[0] - vec1[0]) ** 2 + (vec2[1] - vec1[1]) ** 2
                dists[i + 1][j + 1] = dist
                dist_list.append(dist)
        dist_list = np.array(dist_list)
        thres = np.quantile(dist_list, q=quantile)
        return dists, thres

    @staticmethod
    def detect_blocks(dists, thres):
        """
        Đầu vào:
            - dists, thres: đầu ra của hàm distance_threshold.
        Đầu ra:
            - blocks: list của các list con, mỗi list con là 1 block. List con chứa các số
                nguyên là chỉ số của các hàng trong block.
            Ví dụ: doc ban đầu gồm 50 dòng text, có dạng (list của các dict):
                [dict1, dict2, ... dict50]
            Sau khi detect_blocks thì blocks đầu ra có thể có dạng:
            blocks = [[1, 2, 3, .... 10],
                    [11, 12, 13, ... 20],
                    [21, 22, 23, ... 30],
                    [31, 32, 33, ... 40],
                    [41, 42, 43, ... 50]]
            Như vậy doc được chia thành 5 block, block đầu tiên gồm các chỉ số 1, 2, 3, .. 10
            Do đó block đầu tiên sẽ gồm các dòng [dict1, dict2, ... dict10]
        """
        blocks = []
        while dists.shape[0] > 1:
            bank_len = dists.shape[0] - 1
            sub_dists = dists[1:, 1:]
            block = []
            block_end = False
            i = 1
            while not block_end:
                block.append(dists[i, 0])
                if i < bank_len:
                    more_line = False
                    for j in range(i, bank_len):
                        if sub_dists[i - 1, j] < thres:
                            more_line = True
                            break
                    if more_line:
                        i = j + 1
                    else:
                        block_end = True
                else:
                    block_end = True
            blocks.append(block)
            for idx in block:
                matrix_idx = np.where(dists[:, 0] == idx)
                dists = np.delete(dists, matrix_idx, 0)
                dists = np.delete(dists, matrix_idx, 1)
        return blocks

    @staticmethod
    def detect_para(block, coordinates, feature_vecs2, doc_text):
        """
        Phát hiện đoạn văn trong một block dựa vào lề trái
        Đầu vào:
            - block: list con của blocks - đầu ra của hàm detect_blocks.
            - coordinates, feature_vecs2: đầu ra của hàm get_vecs.
            - doc_text: đầu ra của hàm get_texts.
        Đầu ra:
            - para_list: list của các list. Mỗi list con đại diện cho 1 paragraph trong doc.
                Mỗi list con chứa các số nguyên là chỉ số dòng của các dòng trong paragraph. Ví dụ:
                para_list = [[1,2,3,4], [5,6]], tức là doc ban đầu gồm 2 đoạn văn, đoạn 1 chứa các dòng 1, 2, 3, 4.
                Đoạn 2 chứa các dòng 5, 6. Chỉ số các dòng chính là chỉ số trong doc (list của các dict - mỗi dict là 1 dòng).
            - para_pos: list của các tuple. Mỗi tuple là bộ 4 số (x1, y1, x2, y2) chỉ vị trí của 1 paragraph,
                trong đó (x1, y1) là góc trên bên trái và (x2, y2) là góc dưới bên phải.
            - para_text: list của các strings. Mỗi string là toàn bộ text của 1 paragraph trong doc.
        """
        block_len = len(block)
        left_indents = []
        right_indents = []
        for row_idx in block:
            left_indents.append(feature_vecs2[row_idx][0])  # len(left_indents) == len(block)
            right_indents.append(coordinates[row_idx][2])  # len(right_indents) == len(block)
        left_margin = min(left_indents)
        right_margin = max(right_indents)

        block_width = right_margin - left_margin

        para_marks = []

        for row_idx in block:
            para_marks.append(
                (feature_vecs2[row_idx][0] > left_margin + block_width * 0.05))  # len(para_marks) == len(block)

        para_list = []
        para_pos = []
        para_text = []
        if any(para_marks):
            for i in range(block_len):
                if para_marks[i]:
                    p = [block[i]]
                    p_text = doc_text[block[i]]
                    while (i <= block_len - 2) and (not para_marks[i + 1]):
                        p.append(block[i + 1])
                        p_text = p_text + ' ' + doc_text[block[i + 1]]
                        i += 1
                    x1 = min([coordinates[idx][0] for idx in p])
                    y1 = min([coordinates[idx][1] for idx in p])
                    x2 = max([coordinates[idx][4] for idx in p])
                    y2 = max([coordinates[idx][5] for idx in p])
                    para_list.append(p)
                    para_pos.append((x1, y1, x2, y2))
                    para_text.append(p_text)
        else:
            p = []
            p_text = ''
            for i in range(block_len):
                p.append(block[i])
                p_text = p_text + ' ' + doc_text[block[i]]
            x1 = min([coordinates[idx][0] for idx in p])
            y1 = min([coordinates[idx][1] for idx in p])
            x2 = max([coordinates[idx][4] for idx in p])
            y2 = max([coordinates[idx][5] for idx in p])
            para_list.append(p)
            para_pos.append((x1, y1, x2, y2))
            para_text.append(p_text)

        return para_list, para_pos, para_text

    def get_layout(self, doc):
        """
        Đầu vào:
            - doc: json object: list của các dict. Mỗi dict gồm boundingbox và text
                của 1 dòng chữ. doc = 1 văn bản
        Đầu ra:
            - layout: json object chứa thông tin layout của văn bản doc theo các level:
                + block
                + paragraph
                + line
            Cụ thể, layout = [block1, block2, ...]
            block1 = {"boundingBox": (x1, y1, x2, y2),
                        "block_content":[para1, para2, ...]}
            para1 = {"boundingBox": (x1, y1, x2, y2),
                        "para_content":[line1, line2, ...]}
            line1 = {"boundingBox": (x1, y1, x2, y2),
                        "text": 'Buông đôi tay nhau raaaaaaaaaaaaaaaaaaaa'}
        """
        doc_text = self.get_texts(doc)
        doc_len = len(doc)
        coordinates, feature_vecs, feature_vecs2 = self.get_vecs(doc)
        dists, thres = self.distance_threshold(feature_vecs2, doc_len)
        blocks = self.detect_blocks(dists, thres)
        for i in range(len(blocks)):
            blocks[i] = list(map(int, blocks[i]))

        layout = []
        for block in blocks:
            layout_block = {}
            block_content = []
            para_list, para_pos, para_text = self.detect_para(block, coordinates, feature_vecs2, doc_text)
            num_para = len(para_list)
            for i in range(num_para):
                layout_para = {}
                para_content = []
                for line_idx in para_list[i]:
                    layout_line = {}
                    x1, y1, x2, y2, x3, y3, x4, y4 = doc[line_idx]['boundingBox']
                    layout_line['boundingBox'] = (x1, y1, x3, y3)
                    layout_line['text'] = doc[line_idx]['text']
                    para_content.append(layout_line)
                layout_para['boundingBox'] = para_pos[i]
                layout_para['para_content'] = para_content
                block_content.append(layout_para)
            x_min = min([pos[0] for pos in para_pos])
            y_min = min([pos[1] for pos in para_pos])
            x_max = max([pos[2] for pos in para_pos])
            y_max = max([pos[3] for pos in para_pos])
            layout_block['boundingBox'] = (x_min, y_min, x_max, y_max)
            layout_block['block_content'] = block_content
            layout.append(layout_block)

        return layout


if __name__ == "__main__":
    fnames_json = glob("json_data/*.json")

    raw_data = []

    '''
    Sau bước này, raw_data là một list của các doc.
    Mỗi doc lại là một list của các dict.
    Mỗi dict là một dòng trong doc gồm có 2 key là 'boundingbox' và 'text' 
    '''

    for fname in fnames_json:
        f = open(fname)
        data = json.load(f)
        raw_data.append(data)

    sample = 9

    doc = raw_data[sample]
    layout_engine = RuleBasedLayout()
    layout = layout_engine.get_layout(doc)
    print(layout)


class ClusteringLayout(AbstractLayout):
    def get_layout(self, json_input):
        pass


class DeepLearningLayout(AbstractLayout):
    def get_layout(self, json_input):
        pass
