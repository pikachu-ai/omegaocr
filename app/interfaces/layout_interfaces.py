from abc import ABC, abstractmethod, abstractstaticmethod


class AbstractLayout(ABC):

    @abstractmethod
    def get_layout(self, doc):
        """
        Analysis layout from input json lines.
        :param doc: input json lines.
        :return: json of document structure which contains blocks, paragraphs, lines.
        """
        pass
