from abc import ABC, abstractmethod


class AbstractReconstructor(ABC):

    @abstractmethod
    def get_doc(self, input_dict: dict, meta: dict):
        """
        Interface for Layout Analysis Model
        :param input_dict: input dictionary of extracted information
        :param meta:
        :return: document object (ex: python-docx document)
        """
        pass

    @abstractmethod
    def save_doc(self, input_document, path):
        """
        :param input_document: document object of according library
        :param path: path to saving file
        :return: dictionary of results:
        {
            is_success: bool,
            message: str
        }
        """
        pass


class DocxReconstructor(AbstractReconstructor):
    def get_doc(self, input_dict: dict, meta: dict):
        """
        Interface for Layout Analysis Model
        :param input_dict: input dictionary of extracted information of 1 page
        :param meta: dict, ex:
        {
            'file_type': 'docx'
        }
        :return: document object (ex: python-docx document)
        """
        pass

    def save_doc(self, input_document, path):
        """
        :param input_document: document object of according library
        :param path: path to saving file
        :return: dictionary of results:
        {
            is_success: bool,
            message: str
        }
        """
        pass
