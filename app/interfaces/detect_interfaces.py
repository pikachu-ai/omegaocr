from abc import ABC, abstractmethod, abstractstaticmethod
import numpy as np


class AbstractDetector(ABC):

    @abstractmethod
    def detect(self, img: np.ndarray, meta=None):
        """
        Interface for Layout Analysis Model
        :param img: input image as numpy array object
        :param meta: metadata for analysis
        :return: json object
        """
        pass
