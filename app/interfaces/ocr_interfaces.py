from abc import abstractmethod, ABC
import numpy as np


class AbstractOCR(ABC):
    """
    Base class for wrapper of OCR model
    """

    @abstractmethod
    def preprocess(self, img: np.ndarray):
        """
        Perform some preprocessing for input image.
        """
        pass

    @abstractmethod
    def get_text(self, img: np.ndarray, meta=None) -> str:
        """
        Interface for Layout Analysis Model
        :param img: input image as numpy array object
        :param meta: metadata for ocr extract such as: preprocessing, paragraph or line...
        :return: json object
        """
        pass

    def get_text_para(self, img: np.ndarray, meta: dict) -> list:
        pass


class ModelLoader:
    """
    Base class for ocr model loader.
    """
    @staticmethod
    def load(config):
        pass
