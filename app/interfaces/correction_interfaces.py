from abc import ABC, abstractmethod, abstractstaticmethod


class AbstractCorrection(ABC):

    @abstractmethod
    def correct(self, s: str):
        """
        Interface for Text Correction Engine.
        :param s: input string
        :param meta: metadata for text correction
        :return: corrected string
        """
        pass
