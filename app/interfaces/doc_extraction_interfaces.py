from abc import ABC, abstractmethod


class AbstractDocExtraction(ABC):

    @abstractmethod
    def get_doc_image(self, image, debug_dir):
        """
        Interface for Layout Analysis Model
        :param input_dict: input dictionary of extracted information
        :param meta:
        :return: document object (ex: python-docx document)
        """
        pass


class DocExtraction(AbstractDocExtraction):
    def get_doc_image(self, input_dict, debug_dir):
        """
        Interface for Layout Analysis Model
        :param input_dict: input dictionary of extracted information of 1 page
        :param meta: dict, ex:
        {
            'file_type': 'docx'
        }
        :return: document object (ex: python-docx document)
        """
        pass

    
