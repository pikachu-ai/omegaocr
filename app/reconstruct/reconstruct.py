from interfaces.reconstruct_interfaces import AbstractReconstructor
from docx.document import Document
from docx.shared import Inches, Mm, Pt
from docx import Document
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT, WD_ALIGN_PARAGRAPH
import base64
import cv2
from io import BytesIO
import json
import os
import numpy as np


def make_para(doc, obj, align='justify', page_margin=0, space_before=6, space_after=6, font_size=13, page_width=800):
    """
    Inputs:
        doc: document class from docx
        obj: dict of str, each str is a sentence in paragraph
    Returns: None
        paragraph is added in doc with the specified format
    """
    WIDTH_A4 = 210

    para = doc.add_paragraph()
    run = para.add_run(obj['content'])
    run.font.size = Pt(font_size)
    run.font.name = 'Times New Roman'
    para.paragraph_format.space_before = Pt(space_before)
    para.paragraph_format.space_after = Pt(space_after)

    # set first_line_indent
    real_margin = round(obj['location']['x1'] * WIDTH_A4 / page_width)
    if real_margin - page_margin > 15:
        para.paragraph_format.first_line_indent = Mm(round(obj['location']['x1'] * WIDTH_A4 / page_width) - page_margin)
    elif real_margin - page_margin > 10:
        para.paragraph_format.first_line_indent = Mm(10)

    if align == 'justify':
        para.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.JUSTIFY
    elif align == 'left':
        para.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.LEFT
    elif align == 'right':
        para.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.RIGHT
    else:
        para.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER


def make_heading(doc, content, align='justify'):
    """
    Inputs:
        doc: document object from docx
        content: string of content
        align ('justify', 'left', 'right', 'center'): heading alignment
    Returns: None
        heading is added in document with specified format
    """
    heading = doc.add_heading(content, 0)
    if align == 'justify':
        heading.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.JUSTIFY
    elif align == 'left':
        heading.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.LEFT
    elif align == 'right':
        heading.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.RIGHT
    else:
        heading.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER


def make_table(doc, object1, object2, im_type, page_width):
    """
    Inputs:
        doc: document object from docx
        object1: object in the first cell
        object2: object in the second cell
        align ('justify', 'left', 'right', 'center'): heading alignment
    Returns: None
        table is added in document with specified format
    """
    WIDTH_A4 = 210
    table = doc.add_table(rows=1, cols=2)

    if object1['label'] == 'table_text':
        table.cell(0,0).paragraphs[0].paragraph_format.alignment = WD_ALIGN_PARAGRAPH.CENTER
        run = table.cell(0, 0).paragraphs[0].add_run(object1['content'])
        run.font.size = Pt(13)
        run.font.name = 'Times New Roman'
    elif object1['label'] == 'table_image':
        if im_type == 'b64':
            im_byte = photo_preprocess_b64(object1['content'])
        elif im_type == 'array':
            im_byte = photo_preprocess_np(object1['content'])

        width_image = round((object1['location']['x2'] - object1['location']['x1']) / page_width * WIDTH_A4)
        paragraph = table.cell(0,0).paragraphs[0]
        run = paragraph.add_run()
        run.add_picture(im_byte, width=Mm(width_image))
        paragraph.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.CENTER


    if object2['label'] == 'table_text':
        table.cell(0, 1).paragraphs[0].paragraph_format.alignment = WD_ALIGN_PARAGRAPH.CENTER
        run = table.cell(0, 1).paragraphs[0].add_run(object1['content'])
        run.font.size = Pt(13)
        run.font.name = 'Times New Roman'
    elif object2['label'] == 'table_image':
        if im_type == 'b64':
            im_byte = photo_preprocess_b64(object2['content'])
        elif im_type == 'array':
            im_byte = photo_preprocess_np(object2['content'])

        width_image = round((object2['location']['x2'] - object2['location']['x1']) / page_width * WIDTH_A4)
        paragraph = table.cell(0, 1).paragraphs[0]
        run = paragraph.add_run()
        run.add_picture(im_byte, width=Mm(width_image))
        paragraph.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.CENTER


def is_inline(box1, box2) -> bool:
    """
    Check if 2 box is in a line by compare bottom 2 box
    """
    x1_box1 = box1['location']['x1']
    x2_box1 = box1['location']['x2']
    x1_box2 = box2['location']['x1']
    x2_box2 = box2['location']['x2']

    y1_box1 = box1['location']['y1']
    y2_box1 = box1['location']['y2']
    y1_box2 = box2['location']['y1']
    y2_box2 = box2['location']['y2']

    if y2_box1 - y1_box2 >= 0 and min(x2_box1, x2_box2) - max(x1_box1, x1_box2) <= 0:
        return True
    return False


def label_table(objects):
    """
        Update label to "table" for objects in the same line
        Inputs:
            objects: list of sorted objects
        Return:
            a list of updated label objects
    """

    i = 0
    while i != len(objects) - 2:
        if is_inline(objects[i], objects[i+1]):
            # label to table_image if it is image and table text if table_text
            if objects[i]['label'] == "image":
                objects[i]['label'] = 'table_image'
            elif objects[i]['label'] == "line":
                objects[i]['label'] = 'table_text'

            if objects[i+1]['label'] == "image":
                objects[i+1]['label'] = 'table_image'
            elif objects[i+1]['label'] == "line":
                objects[i+1]['label'] = 'table_text'

            # check what object is first cell
            if objects[i]['location']['x1'] > objects[i+1]['location']['x1']:
                temp = objects[i]
                objects[i] = objects[i+1]
                objects[i+1] = temp

            i = i+2
        else:
            i += 1

    return objects

def photo_preprocess_b64(base64_str: str):
    """
    Inputs:
        base64_str: base 64 string of image
    Returns:
        Bytes object of base64_str, used for function make_photo
    """
    base64_bytes = base64_str.encode('ascii')
    img_bytes = base64.b64decode(base64_bytes)
    im_file = BytesIO(img_bytes)
    return im_file


def photo_preprocess_np(array):
    """
    Inputs:
        array: numpy array of image
    Returns:
        Bytes object of array, used for function make_photo
    """
    array = np.array(array)
    success, encoded_img = cv2.imencode('.jpeg', array)
    im_file = BytesIO(encoded_img)
    return im_file


def make_photo(doc, im_file, width=2000000):
    """
    Inputs:
        doc: docx document
        im_file: bytes object of image
    Returns: None
        photo is put on document
    """
    doc.add_picture(im_file, width=width)


def sort_obj(json_objects):
    """
    Inputs: json object of the form
        {object1: {label: 'str', content: 'dict of str', loc: {x1:,y1:,x2:,y2:}}
        {object2: .....}
        .....
    Returns: list of objects sorted by y1 in ascending order.
    """
    sorted_obj = []
    temp = []

    for key, obj in json_objects.items():
        temp.append([obj['location']['y1'], obj])

    temp.sort(key=lambda x: x[0])
    for obj in temp:
        sorted_obj.append(obj[1])
    return sorted_obj


def get_margin(list_objects, page_width):
    WIDTH_A4 = 210
    all_width = []
    for obj in list_objects:
        all_width.append(obj['location']['x2'] - obj['location']['x1'])

    left_margin_point = list_objects[np.argmax(all_width)]['location']['x1']
    right_margin_point = list_objects[np.argmax(all_width)]['location']['x2']

    # calculate final margin(mm)
    left_margin = round(left_margin_point * WIDTH_A4 / page_width)
    right_margin = WIDTH_A4 - round(right_margin_point * WIDTH_A4 / page_width)

    return left_margin, right_margin

def area(box):
    weight = box['location']['x2'] - box['location']['x1']
    height = box['location']['y2'] - box['location']['y1']
    return weight * height


def check_inside(box1, box2):
    y1_box1 = box1['location']['y1']
    y2_box1 = box1['location']['y2']
    x1_box1 = box1['location']['x1']
    x2_box1 = box1['location']['x2']

    y1_box2 = box2['location']['y1']
    y2_box2 = box2['location']['y2']
    x1_box2 = box2['location']['x1']
    x2_box2 = box2['location']['x2']

    # determine the (x, y)-coordinates of the intersection rectangle
    xA = max(x1_box1, x1_box2)
    yA = max(y1_box1, y1_box2)
    xB = min(x2_box1, x2_box2)
    yB = min(y2_box1, y2_box2)
    # compute the area of intersection rectangle
    interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)
    # compute the area of both the prediction and ground-truth
    # rectangles
    box1Area = area(box1)
    box2Area = area(box2)
    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    overlap_percent = interArea / min(box1Area, box2Area)
    if overlap_percent > 0.9:
        return True
    return False


def remove_duplicate_box(objects: list) -> list:
    remove = []
    for i in range(len(objects) - 2):
        for j in range(i + 1, len(objects)):
            if check_inside(objects[i], objects[j]):
                if area(objects[i]) > area(objects[j]):
                    remove.append(objects[j])
                else:
                    remove.append(objects[i])

    for i in range(len(remove)):
        objects.remove(remove[i])

    return objects


class DocxReconstructor(AbstractReconstructor):
    """
    Docx file Reconstructor.
    """
    def __init__(self):
        pass

    def get_doc(self, input_dict: dict, meta: dict, im_type = 'array') -> Document:
        """
        Reconstruct the document using input data
        :param input_dict: dict, contain input data from controller
        :param meta: some additional information about what to do.
        :im_type: 'b64' or 'numpy' - type specification for image string of input_dict
        :return: python-docx document object
        """
        page_width = input_dict['width']
        input_dict.pop("width")
        list_objects = sort_obj(input_dict) # Sort input dict by coordinate y1 and change to list
        left_margin, right_margin = get_margin(list_objects, page_width)

        document = Document()
        section = document.sections[0]
        section.page_height = Mm(297)
        section.page_width = Mm(210)
        section.left_margin = Mm(left_margin)
        section.right_margin = Mm(right_margin)

        label_table(list_objects)
        remove_duplicate_box(list_objects)

        i = 0
        while i != len(list_objects):
            if list_objects[i]['label'] == 'line':
                make_para(document, list_objects[i], page_margin=left_margin, page_width=page_width)
                i += 1
            elif list_objects[i]['label'] in ["table_image", "table_text"]:
                make_table(document, list_objects[i], list_objects[i+1], im_type, page_width=page_width)
                i += 2
            elif list_objects[i]['label'] == 'image':
                if im_type == 'b64':
                    im_byte = photo_preprocess_b64(list_objects[i]['content'])
                elif im_type == 'array':
                    im_byte = photo_preprocess_np(list_objects[i]['content'])
                make_photo(document, im_byte)
        return document

    def save_doc(self, input_document: Document, path):
        """
        :param path: file path to save document.
        :param input_document: python-docx document object of according library
        :return: dictionary of results:
        {
            is_success: bool,
            message: str
        }
        """
        try:
            input_document.save(path)
            message = 'save file success'
            is_success = True
        except Exception as e:
            message = e
            is_success = False

        results = {
            'is_success': is_success,
            'message': message
        }

        return results


if __name__ == '__main__':
    json_paths = 'C:/Users/namnn12/Documents/Project/omegaocr/app/reconstruct/test/gg_img1_funal.json'
    json_paths = 'C:/Users/namnn12/Documents/Project/omegaocr/app/reconstruct/test/01_VBHN_BGDDT.signed.pdf1.jpg.json'
    # json_paths = 'C:/Users/namnn12/Documents/Project/omegaocr/app/reconstruct/test/unnamed.jpg.json'
    # json_paths = 'C:/Users/namnn12/Documents/Project/omegaocr/app/reconstruct/test/phuc_ap_cv_01-2018-CV-BQT_ve_HSPL_page_3.jpg.json'
    with open(json_paths, encoding='utf8') as f:
        data = json.load(f)


    obj1 = DocxReconstructor()
    result1 = obj1.get_doc(data, meta=dict())
    print(result1)
    print(obj1.save_doc(result1, "demo1.docx"))
    os.system('start demo1.docx')
