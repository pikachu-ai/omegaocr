import json
import cv2
import matplotlib.pyplot as plt

def is_inline(obj1, obj2):
    top_left1 = obj1['bounding_poly']['vertices'][0]
    top_right1 = obj1['bounding_poly']['vertices'][1]
    bottom_right1 = obj1['bounding_poly']['vertices'][2]
    bottom_left1 = obj1['bounding_poly']['vertices'][3]

    top_left2 = obj2['bounding_poly']['vertices'][0]
    top_right2 = obj2['bounding_poly']['vertices'][1]
    bottom_right2 = obj2['bounding_poly']['vertices'][2]
    bottom_left2 = obj2['bounding_poly']['vertices'][3]

    # if (abs(top_left1['y'] - top_left2['y']) <= 1) and abs(bottom_left1['y'] - bottom_left2['y']) <= 1:
    if bottom_left1['y'] - top_left2['y'] < 0:
        return True
    return False


def merge_box(objs):
    """
    Inputs: list of objects
    Returns: list of objects merged.
    """
    # NOTE: need to fix when have more than 1 merged box
    merge_list = objs
    i = 0
    while i != len(merge_list)-1:
        top_left1 = merge_list[i]['bounding_poly']['vertices'][0]
        top_right1 = merge_list[i]['bounding_poly']['vertices'][1]
        bottom_right1 = merge_list[i]['bounding_poly']['vertices'][2]
        bottom_left1 = merge_list[i]['bounding_poly']['vertices'][3]

        top_left2 = merge_list[i+1]['bounding_poly']['vertices'][0]
        top_right2 = merge_list[i+1]['bounding_poly']['vertices'][1]
        bottom_right2 = merge_list[i+1]['bounding_poly']['vertices'][2]
        bottom_left2 = merge_list[i+1]['bounding_poly']['vertices'][3]

        if abs(top_right1['x'] - top_left2['x']) < 5:
            merge_list[i]['description'] = merge_list[i]['description'] + merge_list[i+1]['description']
            merge_list[i]['bounding_poly']['vertices'][1] = merge_list[i+1]['bounding_poly']['vertices'][1]
            merge_list[i]['bounding_poly']['vertices'][2] = merge_list[i+1]['bounding_poly']['vertices'][2]
            merge_list.pop(i+1)

        if abs(top_right1['x'] - top_left2['x']) < 15:
            merge_list[i]['description'] = merge_list[i]['description'] + " " + merge_list[i+1]['description']
            merge_list[i]['bounding_poly']['vertices'][1] = merge_list[i+1]['bounding_poly']['vertices'][1]
            merge_list[i]['bounding_poly']['vertices'][2] = merge_list[i+1]['bounding_poly']['vertices'][2]
            merge_list.pop(i+1)

        else:
            i += 1

    return merge_list


image = cv2.imread('C:/Users/namnn12/Documents/Project/omegaocr/app/reconstruct/test/gg_img1.jpg')
with open('gg_test_response.json', encoding="utf8") as f:
    data = json.load(f)

    text = data['text_annotations']
    full_text = text[0]['description']
    word = data['text_annotations'][1:]
    # lines = full_text.split('/n')

    # merge_list[i]['bounding_poly']['vertices'][0]

    lines = merge_box(word)
    for l in lines:
        coords = []
        for pnt in l['bounding_poly']['vertices']:
            coords.append(pnt['x'])
            coords.append(pnt['y'])
        print(coords)

    for line in text:
        x1 = line['bounding_poly']['vertices'][0]['x']
        y1 = line['bounding_poly']['vertices'][0]['y']
        x2 = line['bounding_poly']['vertices'][2]['x']
        y2 = line['bounding_poly']['vertices'][2]['y']
        if (x2-x1)
        cv2.rectangle(image, (x1, y1), (x2, y2), (0, 255, 0), 4)

    cv2.namedWindow("test", cv2.WINDOW_GUI_NORMAL)
    cv2.imshow('test', image)
    cv2.waitKey(0)