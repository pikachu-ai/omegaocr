import cv2
import numpy as np
import json
import os
from glob import glob

json_paths = glob('/home/tupm/Downloads/Courses/DL/Exercises/omegaocr/train/datasets/*lines.json')
json_paths = ['C:/Users/namnn12/Documents/Project/omegaocr/app/reconstruct/test/gg_img1_funal.json']
json_paths = ['C:/Users/namnn12/Documents/Project/omegaocr/app/reconstruct/test/unnamed.jpg.json']
json_paths = ['C:/Users/namnn12/Documents/Project/omegaocr/app/reconstruct/test/phuc_ap_cv_01-2018-CV-BQT_ve_HSPL_page_3.jpg.json']
json_paths = ['C:/Users/namnn12/Documents/Project/omegaocr/app/reconstruct/test/01_VBHN_BGDDT.signed.pdf1.jpg.json']

def sort_obj(json_objects):
    """
    Inputs: json object of the form
        {object1: {label: 'str', content: 'dict of str', loc: {x1:,y1:,x2:,y2:}}
        {object2: .....}
        .....
    Returns: list of objects sorted by y1 in ascending order.
    """
    sorted_obj = []
    temp = []

    for key, obj in json_objects.items():
        temp.append([obj['location']['y1'], obj])

    temp.sort(key=lambda x: x[0])
    for obj in temp:
        sorted_obj.append(obj[1])
    return sorted_obj


for path in json_paths:
    image_path = 'C:/Users/namnn12/Documents/Project/omegaocr/app/reconstruct/test/image4.jpg'
    # if not os.path.isfile(image_path):
    #     image_path = path.replace('_lines.json', '.png')
    
    # if not os.path.isfile(image_path) or not os.path.isfile(path.replace('lines', 'response')):
    #     continue
    with open(path, encoding='utf8') as f:
        data = json.load(f)

    data.pop("width")
    data = sort_obj(data)
    
    image = cv2.imread(image_path)

    for position in data:
        x1 = position['location']['x1']
        x2 = position['location']['x2']
        y1 = position['location']['y1']
        y2 = position['location']['y2']
        cv2.rectangle(image, (x1, y1), (x2, y2), (0, 255, 0), 2)
    
    cv2.namedWindow("test", cv2.WINDOW_GUI_NORMAL)
    cv2.imshow('test', image)
    cv2.waitKey(0)

