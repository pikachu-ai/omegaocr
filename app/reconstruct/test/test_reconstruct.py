from docx.document import Document
from docx.shared import Mm, Pt
from docx import Document
from docx.enum.text import WD_ALIGN_PARAGRAPH
import json
import os
import base64
from io import BytesIO


def sort_obj(json_objects):
    """
    Inputs: json object of the form
        {object1: {label: 'str', content: 'dict of str', loc: {x1:,y1:,x2:,y2:}}
        {object2: .....}
        .....
    Returns: list of objects sorted by y1 in ascending order.
    """
    sorted_obj = []
    temp = []

    for i, obj in enumerate(json_objects):
        temp.append([obj['boundingBox'][1], obj])

    temp.sort(key=lambda x: x[0])
    for obj in temp:
        sorted_obj.append(obj[1])
    return sorted_obj

def is_inline(box1, box2):
    y1_box1 = box1['boundingBox'][1]
    y2_box1 = y1_box1 + box1['boundingBox'][3]
    y1_box2 = box2['boundingBox'][1]
    y2_box2 = y1_box2 + box2['boundingBox'][3]

    if y2_box1 - y1_box2 >= 0:
        return True
    return False

def area(box):
    weight = box['boundingBox'][2]
    height = box['boundingBox'][3]
    return weight * height


def check_inside(box1, box2):
    y1_box1 = box1['boundingBox'][1]
    y2_box1 = y1_box1 + box1['boundingBox'][3]
    x1_box1 = box1['boundingBox'][0]
    x2_box1 = x1_box1 + box1['boundingBox'][2]

    y1_box2 = box2['boundingBox'][1]
    y2_box2 = y1_box2 + box2['boundingBox'][3]
    x1_box2 = box2['boundingBox'][0]
    x2_box2 = x1_box2 + box2['boundingBox'][2]

    # determine the (x, y)-coordinates of the intersection rectangle
    xA = max(x1_box1, x1_box2)
    yA = max(y1_box1, y1_box2)
    xB = min(x2_box1, x2_box2)
    yB = min(y2_box1, y2_box2)
    # compute the area of intersection rectangle
    interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)
    # compute the area of both the prediction and ground-truth
    # rectangles
    box1Area = area(box1)
    box2Area = area(box2)
    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    overlap_percent = interArea / min(box1Area, box2Area)
    if overlap_percent > 0.9:
        return True
    return False


def remove_duplicate_box(objects: list) -> list:
    remove = []
    for i in range(len(objects) - 1):
        for j in range(i+1, len(objects)):
            if check_inside(objects[i], objects[j]):
                if area(objects[i]) > area(objects[j]):
                    remove.append(objects[j])
                else:
                    remove.append(objects[i])
    
    for i in range(len(remove)):
        objects.remove(remove[i])
    
    return objects
             

def merge_box(objects):
    """
    Inputs: list of objects
    Returns: list of objects merged.
    """
    # NOTE: need to fix when have more than 1 merged box
    for i in range(len(objects)-1):
        if is_inline(objects[i], objects[i+1]):
            x1_box1 = objects[i]['boundingBox'][0]
            x2_box1 = x1_box1 + objects[i]['boundingBox'][2]
            x1_box2 = objects[i+1]['boundingBox'][0]

            if x1_box2 - x2_box1 < 20:
                merged_object = objects[i]
                merged_object['text'] = objects[i]['text'] + " " + objects[i+1]['text']
                objects[i] = merged_object
                objects.pop(i+1)

    return objects

def label_table(objects):
    """
        Update label to "table" for objects in the same line
        Inputs:
            objects: list of sorted objects
        Return:
            a list of updated label objects
    """
    # NOTE: Fake label, remove when have label 
    for i in range(len(objects) - 1):
        objects[i]['label'] = 'paraghaph'
    objects[-1]['label'] = 'image'
    objects[-2]['label'] = 'image'


    for i in range(len(objects) - 1):
        if is_inline(objects[i], objects[i+1]):
            if objects[i]['label'] == "image":
                objects[i]['label'] = 'table_image'
            else:
                objects[i]['label'] = 'table_text'
                
            if objects[i+1]['label'] == "image":
                objects[i+1]['label'] = 'table_image'
            else:
                objects[i+1]['label'] = 'table_text'
                

            if objects[i]['boundingBox'][0] > objects[i+1]['boundingBox'][0]:
                temp = objects[i]
                objects[i] = objects[i+1]
                objects[i+1] = temp

    return objects


if __name__ == '__main__':
    WIDTH = 800
    HEIGHT = 1130
    WIDTH_A4 = 210
    HEIGHT_A4 = 297

    json_paths = 'C:/Users/namnn12/Documents/Project/omegaocr/app/reconstruct/test/gg_img1_lines.json'
    document = Document()
    section = document.sections[0]
    section.page_height = Mm(297)
    section.page_width = Mm(210)
    section.left_margin = Mm(0)
    section.right_margin = Mm(0)

    with open(json_paths, encoding='utf8') as f:
        data = json.load(f)
        data = sort_obj(data)
        # data = merge_box(data)
        remove_duplicate_box(data)
        data = label_table(data)
        

        i = 0
        while i != len(data):
            if data[i].get('label') == 'table_text':
                table = document.add_table(rows=1, cols=2)
                table.cell(0,0).text = data[i]['text']
                table.cell(0,0).paragraphs[0].paragraph_format.alignment = WD_ALIGN_PARAGRAPH.CENTER
                table.cell(0,1).text = data[i+1]['text']
                table.cell(0,1).paragraphs[0].paragraph_format.alignment = WD_ALIGN_PARAGRAPH.CENTER
                i += 2

            elif data[i].get('label') == 'table_image':
                table = document.add_table(rows=1, cols=2)

                base64_str = data[i]['content']
                base64_bytes = base64_str.encode('ascii')
                img_bytes = base64.b64decode(base64_bytes)
                im_file = BytesIO(img_bytes)
                paragraph = table.cell(0,0).paragraphs[0]
                run = paragraph.add_run()
                run.add_picture(im_file, width=Pt(data[i]['boundingBox'][3]))
                paragraph.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.CENTER

                paragraph = table.cell(0,1).paragraphs[0]
                run = paragraph.add_run()
                run.add_picture(im_file, width=Pt(data[i]['boundingBox'][3]))
                paragraph.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.CENTER

                i += 2

            else: 
                text = data[i]['text']
                paragraph = document.add_paragraph(text)
                paragraph.paragraph_format.first_line_indent = Mm(round(data[i]['boundingBox'][0] * WIDTH_A4 / WIDTH))
                paragraph.paragraph_format.space_before = Pt(6)
                paragraph.paragraph_format.space_after = Pt(6)
                i += 1

    document.save("test.docx")
    os.system('start test.docx')