## Document Reconstructor
### 1. Input: Json object
```
INPUT_SAMPLE = {

        "object_1": {
            "label": "header",
            "location": {
                "x1": 300,
                "y1": 200,
                "x2": 600,
                "y2": 250,
            },
            "content": "Trung Thu năm nay"
        },

        "object_2": {
            "label": "paragraph",
            "location": {
                "x1": 50,
                "y1": 350,
                "x2": 800,
                "y2": 740,
            },
            "content": {
                "1": "Ngày ấy, những ngày này, phố xá nơi tôi ở lại huyên náo tiếng múa lân, kèn trống rộn",  # line 1
                "2": "ràng. Háo hức nhất chắc chắn không ai khác là lũ trẻ chúng tôi. Mè nheo nài nỉ bố mẹ",
                "3": "mua cho đèn ông sao, đèn kéo quân, tung tăng chạy khắp xóm khoe bạn bè những món",
                "4": "quà, chia nhau từng miếng bánh dẻo, bánh nướng. Không có đủ đầy mọi thứ bánh kẹo,",
                "5": "nhiều nơi vui chơi như bây giờ, niềm vui lúc ấy thật giản đơn, quãng thời gian vô âu lo",
                "6": "đó lại là những ngày hạnh phúc nhất."
            }
        },

        "object_3": {
            "label": "image",
            "location": {
                "x1": 50,
                "y1": 350,
                "x2": 800,
                "y2": 740,
            },
            "content": image_base64_encoded_string
        }
    }
}
```

#### 2. Output
.docx file. Encode in blob.
Tạm thời phần này lấy output của hàm DocxReconstructor.save_doc().
Khi đóng gói vào API sẽ bổ sung phần truyền file sau.
```

```
