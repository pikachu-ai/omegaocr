def sort_obj(json_objects: dict, sorted_key="y1"):
    """
    Sort a dict by a key
    Inputs:
        json_objects: dict of objects
        sorted_key: to sort by key
    Returns:
        a list of sorted object(dict)
    """
    sorted_obj = []
    temp = []

    for key, obj in json_objects.items():
        temp.append([obj['location'][sorted_key], obj])

    temp.sort(key=lambda x: x[0])
    for obj in temp:
        sorted_obj.append(obj[1])
    return sorted_obj


def update_box_label(objects: list) -> list:
    """
        Update label to "textbox" for objects in the same line
        Inputs:
            objects: list of sorted objects
        Return:
            a list of updated label objects
    """
    DIFFERENCE = 5
    for i in range(len(objects) - 1):
        if objects[i]['location']['y2'] - objects[i + 1]['location']['y1'] < DIFFERENCE:
            objects[i + 1]['label'] = 'textbox'

    return objects


def update_table_label(objects: list) -> list:
    """
        Update label to "table" for objects in the same line
        Inputs:
            objects: list of sorted objects
        Return:
            a list of updated label objects
    """
    DIFFERENCE = 5
    updated_list = []
    for i in range(len(objects) - 1):
        if objects[i]['location']['y2'] - objects[i + 1]['location']['y1'] < DIFFERENCE:
            new_object = {
                'label': "table",
                "location": {
                    "x1": objects[i]['location']['x1'],
                    "y1": objects[i]['location']['y1'],
                    "x2": objects[i + 1]['location']['x2'],
                    "y2": objects[i + 1]['location']['y2'],
                },
                'content': {
                    '1': objects[i],
                    '2': objects[i + 1]
                }
            }
            updated_list.append(new_object)
            i += 1
        elif i == len(objects) - 2:
            updated_list.append(objects[i])
            updated_list.append(objects[i + 1])
        else:
            updated_list.append(objects[i])

    return updated_list
