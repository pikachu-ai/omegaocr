from app.interfaces.correction_interfaces import AbstractCorrection
from app.utils.process_string import NormUnicode
from difflib import get_close_matches
import nltk
import pickle


class NgramCorrection(AbstractCorrection):
    def __init__(self):
        """
        Load dictionary and counter grams of our n-grams language model.
        :param config: DI config
        """
        self.counter_trigram = pickle.load(open('models/counter_tri_gram.pkl', 'r'))
        self.tri_dict = pickle.load(open('models/tri_dict.pkl', 'r'))
        self.maps_uni = {
            line.split('\t')[0]: line.split('\t')[1] for line in open('dictionary/maps_uni.txt', 'r').read().split('\n')
        }
        self.maps_bi = {
            line.split('\t')[0]: line.split('\t')[1] for line in open('dictionary/maps_bi.txt', 'r').read().split('\n')
        }

    @staticmethod
    def clean_string(string):
        """
        Perform some cleaning techniques before correction.
        :return:
        """
        string = NormUnicode.normalize_text(string, remove_accent=False, lower=False)

        return string

    def trigram_correct(self, s: str):
        """
        Correct 3-word string using tri-gram language model.
        :param s: input 3-word string
        :param verbose: verbose mode
        :return: corrected string, if it's already corrected, so the returning string will be maintained.
        """

        s = self.clean_string(s)
        s = s.split()

        seq = ' '.join(s[:-1])  # previous words
        errword = s[-1]  # current word is consider as the error word, for spell checking and correcting

        tokens = nltk.word_tokenize(seq.lower())
        tokens = ['<s>'] + tokens

        i = len(tokens) - 1
        key = (tokens[i - 1], tokens[i])

        dict_nextword = {}
        next_words = self.tri_dict[key]
        for w in next_words:
            dict_nextword[w] = self.counter_trigram[key + (w,)]
        sorted_dict = sorted(dict_nextword, key=dict_nextword.__getitem__, reverse=True)

        closed_words = get_close_matches(errword, possibilities=sorted_dict, n=3, cutoff=0.4)

        return seq + ' ' + closed_words[0]

    def correct(self, input_string: str):

        """
        Implementations of correct method.
        :param input_string: input string of arbitrary length
        :return: corrected string
        """
        s = self.clean_string(input_string)

        for key, val in self.maps_uni:
            s = s.replace(key, val)
        for key, val in self.maps_bi:
            s = s.replace(key, val)

        s_list = s.split()

        tri_grams = nltk.ngrams(s_list, n=3)
        n = len(tri_grams)

        # do the correction for each tri_grams
        out = list()
        out.extend(list(self.trigram_correct(' '.join(tri_grams[0]))))  # for the first phrase
        for i in range(1, n):
            out.append(self.trigram_correct(' '.join(tri_grams[i]))[-1])

        return ' '.join(out)


class NoisyChannelCorrection(AbstractCorrection):
    """
    Implemtation of Peter Norvig's Spell Correction.
    """
    def __init__(self):
        pass

    def correct(self, s: str, meta=None):
        pass
