import cv2
import os

def draw_debug(image, bboxs, path):
    for idx, component in enumerate(bboxs):
        x1, y1, x2, y2 = component['location'].values()
        cv2.rectangle(image, (x1, y1), (x2, y2), (0, 255, 0), 1)
    out_path = path.replace('/outputs', '/debug').replace('.docx', '.jpg')
    cv2.imwrite(out_path, image)