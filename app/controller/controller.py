import json
from abc import ABC, abstractmethod

from interfaces.detect_interfaces import AbstractDetector
from interfaces.ocr_interfaces import AbstractOCR
from interfaces.correction_interfaces import AbstractCorrection
from interfaces.reconstruct_interfaces import AbstractReconstructor
from interfaces.layout_interfaces import AbstractLayout
from interfaces.doc_extraction_interfaces import AbstractDocExtraction

from tqdm import tqdm
from time import time
from utils.image_tools import crop_rectangle
from controller.debuger import draw_debug
import base64
import cv2


class AbstractController(ABC):
    """
    Controller Base Class.
    """
    @abstractmethod
    def gen_docx(self, image):
        pass


class VanillaController(AbstractController):
    """
    Step by step Controller.
    """
    def __init__(self, detect_engine: AbstractDetector,
                 ocr_engine: AbstractOCR,
                 correct_engine: AbstractCorrection,
                 layout_engine: AbstractLayout,
                 reconstructor: AbstractReconstructor,
                 extractor: AbstractDocExtraction):

        self.extractor = extractor
        self.detect_engine = detect_engine
        self.ocr_engine = ocr_engine
        self.correct_engine = correct_engine
        self.layout_engine = layout_engine
        self.reconstructor = reconstructor

    def gen_docx(self, image, output_path='save.docx'):
        t1 = time()
        image = self.extractor.get_doc_image(image)
        layout = self.detect_engine.detect(image.copy())
        t2 = time()
        draw_debug(image.copy(), layout, output_path)
        input_docx = {}
        input_docx['width'] = image.shape[1]
        input_docx['height'] = image.shape[0]
        input_docx['datalist'] = []

        for idx, component in tqdm(enumerate(layout)):
            x1, y1, x2, y2 = component['location'].values()
            if x2-x1 < 32:
                x2 = x1 + y2-y1 + 3
            cut_img = crop_rectangle(image, box=(x1, max(0, y1-3), x2, y2))
            if component['label'] == 'line':
                component['content'] = self.ocr_engine.get_text(cut_img)
                component['content'] = self.correct_engine.correct(component['content'])
            else:
                # component['content'] = cut_img.tolist()
                status, d = cv2.imencode('.jpg', cut_img)
                cv2.imwrite("test.png", cut_img)
                with open('test.png', 'rb') as f:
                    component['content'] = base64.b64encode(f.read()).decode('utf-8')
                # component['content'] = base64.b64encode(d).decode('utf-8')
            # input_docx[idx] = component
            input_docx['datalist'].append(component)

        # layout analysis
        input_docx = self.layout_engine.get_layout(input_docx)

        t3 = time()
        print('detecting time: ', t2-t1)
        print("ocr time: ", t3-t2)
        with open(output_path.replace('.docx', '.json'), 'w') as f:
            json.dump(input_docx, f)
        # docs = self.reconstructor.get_doc(input_docx, {}, im_type='array')
        # self.reconstructor.save_doc(docs, output_path)
        t4 = time()
        print('reconstruct time: ', t4-t3)
        print('total time: ', t4-t1)
        # return json.dumps(input_docx)
        return input_docx
