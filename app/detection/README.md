## Document Layout
#### 1. Input: numpy image

```
META_DATA = {
    "property_1": "value1"
    ...
}
```
### 2. Output: Json object
```
OUTPUT_SAMPLE = [
    {
        "label": "line",
        "location": {
            "x1": 300,
            "y1": 200,
            "x2": 600,
            "y2": 250,
        }
    },
    {
        "label": "line",
        "location": {
            "x1": 50,
            "y1": 350,
            "x2": 800,
            "y2": 740,
        }
    },
    {
        "label": "image",
        "location": {
            "x1": 50,
            "y1": 350,
            "x2": 800,
            "y2": 740,
        }
    }
]
```
# Setup cho phần layout
    * Link tải model: https://drive.google.com/drive/u/0/folders/1SsUFEkNbQ80MuGkMMkLSATcSIv-qu7qT