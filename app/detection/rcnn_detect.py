import numpy as np
from interfaces.detect_interfaces import AbstractDetector
from detection.lib.resnet.detector import DetectorTF2
from detection.config import RESNET50_PATH


class ResnetDetector(AbstractDetector):
    def __init__(self):
        self.model = DetectorTF2(RESNET50_PATH)

        self.detect(np.zeros((1000, 600, 3), dtype='uint8'))
    
    def detect(self, img: np.ndarray, meta: dict={}):
        bbox = self.model.DetectFromImage(img)
        output = []
        for x, y, w, h, l in bbox:
            output.append({
                "label": l,
                "location": {
                    "x1": x,
                    "y1": y,
                    "x2": x+w,
                    "y2": y+h,
                }
            })
        return output