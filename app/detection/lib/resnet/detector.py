import cv2
import numpy as np
import tensorflow as tf
from nms import nms


class DetectorTF2:

	def __init__(self, path_to_checkpoint, class_id=None, threshold=0.5):
		# class_id is list of ids for desired classes, or None for all classes in the labelmap
		self.class_id = class_id
		self.Threshold = threshold
		# Loading label map
		self.categories = ["line", "image"]
		# tf.config.experimental.set_visible_devices([], device_type='GPU')
		tf.keras.backend.clear_session()
		self.detect_fn = tf.saved_model.load(path_to_checkpoint)

	def get_mark_mask(self, adaptive):
		num_labels, labels, stats, centroids = cv2.connectedComponentsWithStats(255 - adaptive , 8 , cv2.CV_32S)

		mask = np.zeros(adaptive.shape, dtype=np.uint8)

		for idx, stat in enumerate(stats):
			x, y, w, h, c = stat

			if w > 50 and h > 50 and h < 500:
				# print(idx)
				mask[labels==idx] = 255
		
		return mask

	def preprocessing(self, document_image):
		adaptive = cv2.adaptiveThreshold(cv2.cvtColor(document_image, cv2.COLOR_BGR2GRAY), 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)
		mark_mask = self.get_mark_mask(adaptive)
		adaptive = cv2.medianBlur(adaptive, 3)
		document_image[adaptive==255] = 255
		document_image[mark_mask==255] = np.array([0, 0, 255])
		# document_image = np.where(adaptive==255, 255, document_image)

		return document_image

	def DetectFromImage(self, img):
		img = self.preprocessing(img.copy())
		im_height, im_width, _ = img.shape
		# Expand dimensions since the model expects images to have shape: [1, None, None, 3]
		input_tensor = np.expand_dims(img, 0)
		detections = self.detect_fn(input_tensor)

		bboxes = detections['detection_boxes'][0].numpy()
		bclasses = detections['detection_classes'][0].numpy().astype(np.int32)
		bscores = detections['detection_scores'][0].numpy()
		det_boxes = self.ExtractBBoxes(bboxes, bclasses, bscores, im_width, im_height)

		return det_boxes


	def ExtractBBoxes(self, bboxes, bclasses, bscores, im_width, im_height):
		bbox = []
		scores = []
		classes = []
		for idx in range(len(bboxes)):
			if self.class_id is None or bclasses[idx] in self.class_id:
				if bscores[idx] >= self.Threshold:
					y_min = int(bboxes[idx][0] * im_height)
					x_min = int(bboxes[idx][1] * im_width)
					y_max = int(bboxes[idx][2] * im_height)
					x_max = int(bboxes[idx][3] * im_width)
					class_label = self.categories[int(bclasses[idx])-1]
					bbox.append([x_min, y_min, x_max-x_min, y_max-y_min])
					scores.append(float(bscores[idx]))
					classes.append(class_label)
		idxs = nms.boxes(bbox, [1]*len(bbox))
		# idxs = range(len(bbox))
		# return np.array(bbox)[idxs].tolist()
		bboxes = []
		for idx in idxs:
			bbox[idx].append(classes[idx])
			bboxes.append(bbox[idx])

		return bboxes


	def DisplayDetections(self, image, boxes_list, det_time=None):
		if not boxes_list: return image  # input list is empty
		img = image.copy()
		for idx in range(len(boxes_list)):
			x_min = boxes_list[idx][0]
			y_min = boxes_list[idx][1]
			x_max = boxes_list[idx][2]
			y_max = boxes_list[idx][3]
			cls =  str(boxes_list[idx][4])
			score = str(np.round(boxes_list[idx][-1], 2))

			text = cls + ": " + score
			cv2.rectangle(img, (x_min, y_min), (x_max, y_max), (0, 255, 0), 5)
			cv2.rectangle(img, (x_min, y_min - 20), (x_min, y_min), (255, 255, 255), -1)
			cv2.putText(img, text, (x_min + 5, y_min - 7), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 1)

		if det_time != None:
			fps = round(1000. / det_time, 1)
			fps_txt = str(fps) + " FPS"
			cv2.putText(img, fps_txt, (25, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 0, 0), 2)
		return img

if __name__ == "__main__":
    detector = DetectorTF2("models/resnet50/saved_model")
    image = cv2.imread("images/0001.jpg")
    bboxs = detector.DetectFromImage(image)
    img = detector.DisplayDetections(image, bboxs)
    cv2.namedWindow('', cv2.WINDOW_NORMAL)
    cv2.imshow('', img)
    cv2.waitKey(0)