# import os
from sys import platform

from .models import *
from .utils.datasets import *
from .utils.utils import *


class Infer:
    '''
    Class for main inference

    Args:
        verbose (int): Set verbosity levels
                        0 - Print Nothing
                        1 - Print desired details
    '''

    def __init__(self, verbose=1):
        self.system_dict = {"verbose": verbose, "local": {}, "params": {}}

        self.system_dict["params"]["output"] = "output"

        self.system_dict["params"]["fourcc"] = "mp4v"
        self.system_dict["params"]["half"] = False
        self.system_dict["params"]["device"] = "0"
        self.system_dict["params"]["agnostic_nms"] = False
        self.system_dict["params"]["classes"] = ""
        self.system_dict["params"]["device"] = "0"

        self.system_dict["params"]["view_img"] = False
        self.system_dict["params"]["save_txt"] = True
        self.system_dict["params"]["save_img"] = True

        self.system_dict["params"]["cfg"] = "custom_data/ship/yolov3.cfg"
        self.system_dict["params"]["names"] = "custom_data/ship/classes_list.txt"
        self.system_dict["params"]["weights"] = "weights/last.pt"

        self.system_dict["params"]["img_size"] = 416
        self.system_dict["params"]["conf_thres"] = 0.3
        self.system_dict["params"]["iou_thres"] = 0.5

    def Model(self, model_name, class_list, weight, use_gpu=True, input_size=416, half_precision=False):
        '''
        User function: Set Model parameters

            Available Models
                yolov3
                yolov3s
                yolov3-spp
                yolov3-spp3
                yolov3-tiny
                yolov3-spp-matrix
                csresnext50-panet-spp


        Args:
            model_name (str): Select model from available models
            class_list (list): List of classes as per given in training session
            weight (srt): Path to file storing model weights 
            use_gpu (bool): If True, model is loaded onto GPU device, else on CPU
            half_precision (bool): If True, uses only 16 bit floating point operations for faster inferencing

        Returns:
            None
        '''
        self.system_dict["params"]["cfg"] = model_name + ".cfg"
        self.system_dict["params"]["half"] = half_precision
        self.system_dict["params"]["names"] = class_list
        self.system_dict["params"]["weights"] = weight
        self.system_dict["params"]["use_gpu"] = use_gpu
        self.system_dict["params"]["img_size"] = input_size

        self.system_dict["local"]["device"] = torch_utils.select_device(
            device=self.system_dict["params"]["device"] if use_gpu else 'cpu')

        self.system_dict["local"]["model"] = Darknet(self.system_dict["params"]["cfg"],
                                                     self.system_dict["params"]["img_size"])

        # Load weights
        attempt_download(self.system_dict["params"]["weights"])
        if self.system_dict["params"]["weights"].endswith('.pt'):  # pytorch format
            self.system_dict["local"]["model"].load_state_dict(torch.load(self.system_dict["params"]["weights"],
                                                                          map_location=self.system_dict["local"][
                                                                              "device"])['model'])
        else:  # darknet format
            load_darknet_weights(self.system_dict["local"]["model"],
                                 self.system_dict["params"]["weights"])

        self.system_dict["local"]["model"].to(self.system_dict["local"]["device"]).eval()

        # Half precision
        self.system_dict["params"]["half"] = self.system_dict["params"]["half"] and self.system_dict["local"][
            "device"].type != 'cpu'  # half precision only supported on CUDA
        if self.system_dict["params"]["half"]:
            self.system_dict["local"]["model"].half()

    def Predict(self, image, conf_thres=0.3, iou_thres=0.5):
        '''
        User function: Run inference on image and visualize it

        Args:
            img_path (str): Relative path to the image file
            conf_thres (float): Threshold for predicted scores. Scores for objects detected below this score will not be displayed 
            iou_thres (float): Threshold for bounding boxes nms merging

        Returns:
            None.
        '''
        self.system_dict["params"]["conf_thres"] = conf_thres
        self.system_dict["params"]["iou_thres"] = iou_thres

        processed_image = preprocess_image(
            image,
            img_size=self.system_dict["params"]["img_size"],
            half=self.system_dict["params"]["half"]
        )

        # Get names and colors
        names = self.system_dict["params"]["names"]
        # colors = [[random.randint(0, 255) for _ in range(3)] for _ in range(len(names))]

        # Run inference
        t0 = time.time()
        img = torch.from_numpy(processed_image).to(self.system_dict["local"]["device"])
        img = img.unsqueeze(0)
        pred = self.system_dict["local"]["model"](img)[0]
        pred = non_max_suppression(
            pred, self.system_dict["params"]["conf_thres"],
            self.system_dict["params"]["conf_thres"],
            classes=self.system_dict["params"]["classes"],
            agnostic=self.system_dict["params"]["agnostic_nms"]
        )[0]
        
        pred = pred.cpu().detach().numpy()
        print(processed_image.shape, image.shape)
        # for det in pred:
        pred[:, :4] = scale_coords(processed_image.shape[1:], pred[:, :4], image.shape).round()
        pred = pred.astype(float)
        return [{
            "label": p[-1],
            "location": {
                "x1": p[0],
                "y1": p[1],
                "x2": p[2],
                "y2": p[3],
            }
        } for p in pred]
