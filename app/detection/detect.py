import numpy as np
import sys
import os

PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))
from interfaces.detect_interfaces import AbstractDetector
from detection.rcnn_detect import ResnetDetector


class RCNNDetector(AbstractDetector):
    """
    Wrapper for Faster RCNN Detection Model.
    """

    def __init__(self):
        self.main_layout = ResnetDetector()

    def detect(self, img: np.ndarray, meta: dict = None):
        return self.main_layout.detect(img, meta)


if __name__ == "__main__":
    import cv2
    import json
    import time
    img_path1 = "C:\\DATA\\VB2\\omegaocr\\app\\detection\\images\\test.jpg"
    detector = RCNNDetector()
    tic = time.time()
    image = cv2.imread(img_path1)
    results = detector.detect(image)
    # print(json.dumps(results))
    print(time.time()-tic)
