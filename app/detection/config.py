CLASS_PATH = "models/classes.txt"
MODEL_NAME = "models/yolov3"
WEIGHT_PATH = "models/dla_yolov3.pt"
INPUT_SIZE=416
USE_GPU = False
IOU_THRES = 0.9
CONF_THRES = 0.3

RESNET50_PATH='C:\\DATA\\VB2\\omegaocr\\app\\detection\\models\\resnet50\\saved_model'