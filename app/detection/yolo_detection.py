import numpy as np
import os
import cv2
import sys
from interfaces.detect_interfaces import AbstractDetector
from detection.lib.yolov3.infer_detector import Infer
from detection.config import MODEL_NAME, WEIGHT_PATH, INPUT_SIZE, USE_GPU, CONF_THRES, IOU_THRES, CLASS_PATH


class YoloLayoutAnalysis(AbstractDetector):
    def __init__(self):
        self.load_config()
        # self._model = self.load()

    @property
    def _model(self):
        gtf = Infer()
        gtf.Model(self.model_name, self.class_list, self.weight_path, use_gpu=USE_GPU, input_size=INPUT_SIZE)
        return gtf

    def detect(self, img: np.ndarray, meta: dict = None):
        return self._model.Predict(img, conf_thres=CONF_THRES, iou_thres=IOU_THRES)

    def load_config(self):
        for e in sys.path:
            if os.path.isfile(os.path.join(e, CLASS_PATH)):
                self.model_name = os.path.join(e, MODEL_NAME)
                self.weight_path = os.path.join(e, WEIGHT_PATH)
                self.class_path = os.path.join(e, CLASS_PATH)
                f = open(self.class_path)
                self.class_list = f.readlines()
                f.close()
                return True
        return False


if __name__ == "__main__":
    import json
    img_path1 = "images/PMC5624106_00000.jpg"
    image = cv2.imread(img_path1)
    detector = YoloLayoutAnalysis()
    results = detector.detect(image)
    print(json.dumps(results))