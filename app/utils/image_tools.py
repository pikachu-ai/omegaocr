"""
Tools library for manipulating image in different type
"""


def crop_rectangle(img, box=(None, None, None, None)):
    x1, y1, x2, y2 = box
    return img[y1:y2, x1:x2, :]
