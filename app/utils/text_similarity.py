import numpy as np


def lcs(S: str, T: str):
    m = len(S)
    n = len(T)
    counter = [[0] * (n + 1) for x in range(m + 1)]  # np.zeros((m+1, n+1))
    longest = 0
    lcs_set = set()
    for i in range(m):
        for j in range(n):
            if S[i] == T[j]:
                c = counter[i][j] + 1
                counter[i + 1][j + 1] = c
                if c > longest:
                    lcs_set = set()
                    longest = c
                    lcs_set.add(S[i - c + 1:i + 1])
                elif c == longest:
                    lcs_set.add(S[i - c + 1:i + 1])

    return lcs_set


def get_lcs_lambda(string1):
    return lambda string2: len(lcs(string1, string2).pop()) if len(lcs(string1, string2)) else 0


if __name__ == "__main__":
    string1 = ' m   hà nội'
    string2 = 'adfda hà hà nội'

    print(f"""Longest common sub-string of '{string1}' and '{string2}' is '{lcs(string1, string2)}'""")
    print(get_lcs_lambda(string1)(string2))


def max_length_match(a: str, b: str):
    """
    Calculate our custom distance of 2 strings
    """
    m = len(a)
    n = len(b)

    if n > 0 and b[0] == '^':
        # process special cases
        similarity_rate = 1.0
        special_index = -1 * np.random.randint(1, 100000)  # To make sure it's different over time
        return [similarity_rate, [special_index], [special_index]]

    D = np.zeros((m + 1, n + 1), dtype=int)

    for i in range(1, m + 1):
        for j in range(1, n + 1):
            if a[i - 1] == b[j - 1]:
                replacement_cost = 1
            else:
                replacement_cost = -1
            # Adding 1 to account for the cost of operation
            insertion = -1 + D[i][j - 1]
            deletion = -1 + D[i - 1][j]
            replacement = replacement_cost + D[i - 1][j - 1]

            # Choosing the best option:
            D[i][j] = np.max([insertion, deletion, replacement, 0])

    max_row_indices = [i for i, val in enumerate(np.max(D, axis=1)) if val == np.max(np.max(D, axis=1))]
    max_col_indices = [i for i, val in enumerate(np.max(D, axis=0)) if val == np.max(np.max(D, axis=0))]

    if n == 0:
        similarity_rate = 0
    else:
        similarity_rate = np.max(D)/n
    return [similarity_rate, max_row_indices, max_col_indices]  # (max, line of max)


def get_le_lambda(string1: str):
    return lambda string2: max_length_match(string1, string2)


class TextDistance:
    @staticmethod
    def levenshtein(string_1, string_2):
        len_1 = len(string_1)
        len_2 = len(string_2)
        result_matrix = np.zeros((len_1 + 1, len_2 + 1), dtype=int)
        result_matrix[:, 0] = np.arange(len_1 + 1)  # first col
        result_matrix[0, :] = np.arange(len_2 + 1)  # first line

        for i in range(1, len_1 + 1):
            for j in range(1, len_2 + 1):
                if string_1[i - 1] == string_2[j - 1]:
                    # match
                    cost = 0
                else:
                    # replace
                    cost = 1
                # minimize cost
                result_matrix[i][j] = np.min([result_matrix[i - 1][j] + 1,
                                              result_matrix[i][j - 1] + 1,
                                              result_matrix[i - 1][j - 1] + cost])

        return result_matrix[len_1, len_2]  # (max, line of max)

    @staticmethod
    def wagner_fischer(string1, string2):
        n = len(string1) + 1  # counting empty string 
        m = len(string2) + 1  # counting empty string

        # initialize D matrix
        D = np.zeros(shape=(n, m), dtype=np.int)
        D[:, 0] = range(n)
        D[0, :] = range(m)

        # B is the backtrack matrix. At each index, it contains a triple
        # of booleans, used as flags. if B(i,j) = (1, 1, 0) for example,
        # the distance computed in D(i,j) came from a deletion or a
        # substitution. This is used to compute backtracking later.
        B = np.zeros(shape=(n, m), dtype=[("del", 'b'),
                                          ("sub", 'b'),
                                          ("ins", 'b')])
        B[1:, 0] = (1, 0, 0)
        B[0, 1:] = (0, 0, 1)

        for i, l_1 in enumerate(string1, start=1):
            for j, l_2 in enumerate(string2, start=1):
                deletion = D[i - 1, j] + 1
                insertion = D[i, j - 1] + 1
                substitution = D[i - 1, j - 1] + (0 if l_1 == l_2 else 2)

                mo = np.min([deletion, insertion, substitution])

                B[i, j] = (deletion == mo, substitution == mo, insertion == mo)
                D[i, j] = mo
        return D, B

    @staticmethod
    def back_trace(B_matrix):
        i, j = B_matrix.shape[0] - 1, B_matrix.shape[1] - 1
        backtrace_idxs = [(i, j)]

        while (i, j) != (0, 0):
            if B_matrix[i, j][1]:
                i, j = i - 1, j - 1  # replacement
            elif B_matrix[i, j][0]:
                i, j = i - 1, j  # deletion
            elif B_matrix[i, j][2]:
                i, j = i, j - 1  # insertion
            backtrace_idxs.append((i, j))

        return backtrace_idxs

    @staticmethod
    def find_ops(D, backtrace):
        ops = {'match': 0, 'replace': 0, 'insert': 0, 'delete': 0}

        for step in range(len(backtrace) - 1):  # traverse to n-1 element
            i, j = backtrace[step]
            i_next, j_next = backtrace[step + 1]
            if i + j - sum(backtrace[step + 1]) == 2:  # replace or match
                if D[i - 1][j - 1] - D[i][j] == 1:  # replace
                    ops['replace'] += 1
                else:  # match
                    ops['match'] += 1
            else:  # insert or delete
                if i - i_next:  # delete
                    ops['delete'] += 1
                else:  # insert
                    ops['insert'] += 1

        return ops

    @staticmethod
    def operation_score(ops):
        score = 0
        score = ops['match'] - ops['replace'] - ops['insert'] - ops['delete']
        return score

    @classmethod
    def levenshtein_based_score(cls, string1, string2):
        """return levenshtein distance and transform operation score"""
        D, B = cls.wagner_fischer(string1, string2)
        backtrace = cls.back_trace(B)
        ops = cls.find_ops(D, backtrace)
        score = cls.operation_score(ops)

        return score
