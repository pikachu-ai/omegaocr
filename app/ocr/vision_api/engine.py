from google.cloud import vision
import json
import proto
import time
import os
import logging
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle, Polygon
from google.cloud import vision_v1


logging.basicConfig(filename='gg_annotate_ocr.log', filemode='w',
                    format='%(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger()
logger.info("Google API OCR request logs")


def get_lines_bounding(response_json):
    line_boxes = []
    lines = [{'boundingBox': line['boundingBox'], 'words': line['words']} for region in response_json["regions"] for
             line in region["lines"]]
    #     return lines
    for line in lines:
        bbox = [int(num) for num in line['boundingBox'].split(",")]
        text = ' '.join([word['text'] for word in line['words']])
        line_boxes.append({'boundingBox': bbox, 'text': text})

    return line_boxes


def save_json(obj, full_file_path):
    with open(full_file_path, 'w', encoding='utf8') as file:
        json.dump(obj, file, indent=2, ensure_ascii=False)


def drop_extension(file_path):
    """
    Drop file extension of a file path.
    """
    return os.path.splitext(file_path)[0]


def get_file_path_by_type(path, extensions=('.jpg', '.png')):
    img_paths = []
    for x in os.listdir(path):
        for ext in extensions:
            if x.endswith(ext):
                img_paths.append(path + '/' + x)

    return img_paths


def get_all_files(path):
    file_paths = []
    for x in os.listdir(path):
        file_paths.append(path + '/' + x)

    return file_paths


def show_polygons(image, polygons: dict, figsize=(16, 16), line_color='y'):
    plt.figure(figsize=figsize)
    ax = plt.imshow(image)
    for polygon in polygons:
        bound = polygon["boundingBox"]
        vertices = [(bound[i], bound[i + 1])
                    for i in range(0, len(bound), 2)]

        patch = Polygon(vertices, closed=True, fill=False, linewidth=2, color=line_color)
        ax.axes.add_patch(patch)
        # text = polygon['text']
        # plt.text(vertices[0][0], vertices[0][1], text, fontsize=20, va="top")
    plt.show()


def convert_to_flat_points(point_dict: dict) -> list:
    """
    Flatten the point list from google text-detection API response.
    :param point_dict: dict of points:
    example: {
        'top_left': {'x': 1, 'y': 2},
        'top_right': {...},
        'bottom_right': {...},
        'bottom_left': {...}
    }
    :return: list of point in order: top_left, top_right, bottom_right, bottom_left:
    example: [
      120,
      57,
      345,
      53,
      345,
      78,
      121,
      82
    ]
    """
    top_left = point_dict['top_left']
    top_right = point_dict['top_right']
    bottom_right = point_dict['bottom_right']
    bottom_left = point_dict['bottom_left']

    box = []
    box.extend(list(top_left.values()))
    box.extend(list(top_right.values()))
    box.extend(list(bottom_right.values()))
    box.extend(list(bottom_left.values()))

    return box


def parse_lines_google_ocr(annotation: dict) -> list:
    """
    Parse lines from google text_detection api.
    :param annotation: full text annotation body
    :return: list of lines with boundingBox and text.
    Example: {
        'bounding
    }
    """
    # paragraphs = []
    lines = []

    for page in annotation['pages']:
        for block in page['blocks']:
            for paragraph in block['paragraphs']:
                # para = ""
                line_text = ""
                line_bounding_box = dict()
                for word in paragraph['words']:
                    for symbol in word['symbols']:
                        if line_text == '':  # new line
                            line_bounding_box = dict()
                            line_bounding_box['top_left'] = symbol['bounding_box']['vertices'][0]
                            line_bounding_box['bottom_left'] = symbol['bounding_box']['vertices'][3]

                        line_text += symbol['text']
                        # not all symbols have a detected_break attribute,
                        # in this case we simply add it to the line
                        # if a symbol has a detected_break so it may be space or new line.
                        try:
                            if symbol['property']['detected_break']['type_'] == 1:  # SPACE
                                line_text += ' '
                            elif any([symbol['property']['detected_break']['type_'] == 3,  # EOL_SURE_SPACE:
                                      symbol['property']['detected_break']['type_'] == 4,
                                      symbol['property']['detected_break']['type_'] == 5]):  # LINE_BREAK:
                                line_bounding_box['top_right'] = symbol['bounding_box']['vertices'][1]  # end of line
                                line_bounding_box['bottom_right'] = symbol['bounding_box']['vertices'][2]
                                lines.append({
                                    'boundingBox': convert_to_flat_points(line_bounding_box),
                                    'text': line_text
                                })
                                # para += line + ' '  # wrap text of a paragraph
                                line_text = ''  # new line, reset text and boundingBox

                            else:
                                raise Warning('Invalid detected_break type, type: %s' %
                                              symbol['property']['detected_break']['type_'])

                        except KeyError:
                            pass
                # paragraphs.append(para)
    return lines


def detect_text(image_data):
    """
    Detects text in the file.
    :param image_data: image bytes stream.
    """
    client = vision.ImageAnnotatorClient()

    image = {'content': image_data}
    features = [{"type_": vision_v1.Feature.Type.DOCUMENT_TEXT_DETECTION}]
    request = {'image': image, 'features': features}

    # Performs label detection on the image file
    response = client.annotate_image(request=request)

    results = proto.Message.to_dict(response)

    return results


def get_text_layout(img_data)
