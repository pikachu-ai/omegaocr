import torch
from ocr.crnn.crnn import CRNN
from interfaces.ocr_interfaces import ModelLoader
from ocr.config import Seq2SeqConfig


class Seq2SeqLoader(ModelLoader):
    """
    Static Class Loader for CRNN model.
    """

    @staticmethod
    def load(config: Seq2SeqConfig):
        pass
