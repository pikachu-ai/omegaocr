# OCR Packages
Perform Text Recognition on line image.
For the sake of this DL course project, we don't have enough time for fine tuning the OCR model, our effort has spend almost on the Document Object Detection stage, the OCR model which has been using is trained on our datasets from pre-trained model taken from <a href="https://github.com/pbcquoc/vietocr">this repo</a>.

## Dataset
We collect our own dataset which is well explained in our <a href="https://gitlab.com/pikachu-ai/omegaocr/-/tree/dev/slides%20and%20reports">report</a>. It contains over <b>250.000 text line images</b>.

## Deep Learning Models
- At first, we test the CRNN model, taken from this <a href="https://github.com/pbcquoc/crnn">repo</a>, and the Sequence to Sequence, Transformer model taken from <a href=https://github.com/pbcquoc/vietocr>this</a>, just to have a look. <br>
Despite it was trained on a dataset of milions text line images (as the author said), it returns a poor result on our test set. May be our datasets don't have the same distribution. <br>
Base from these results, we decide to <u>train the OCR model on our own dataset for a reasonable performance<u>.
<br> <br>

- **Sequence to Sequence Model**

<img src="images/seq2seq.png">

- **Transformer Model**

<img src="images/transformer.jpg">

