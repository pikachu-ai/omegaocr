# -*- coding: utf-8 -*-
from abc import ABC, abstractmethod
import os
from vietocr.tool.config import Cfg


class AbstractConfig(ABC):
    """
    Model Config Base Class.
    """
    pass


class CRnnConfig(AbstractConfig):
    """
    Config for CRnn OCR Model.
    """

    model_path = "ocr/models/CRNN_CTC_1.pth"
    char_path = "ocr/lib/char"
    if not os.path.isfile(char_path):
        model_path = os.path.join('app', model_path)
        char_path = os.path.join('app', char_path)
    alphabet = open(char_path, encoding='utf8').read().rstrip()
    nclass = len(alphabet) + 1


class Seq2SeqConfig(AbstractConfig):
    """
    Config for Seq2Seq Model.
    """

    @staticmethod
    def get_config():
        config = Cfg.load_config_from_name('vgg_seq2seq')

        weights = "ocr/models/vgg_seq2seq_1m.pth"  # Use same config as above, but load in trained weights.
        if not os.path.isfile(weights):
            weights = os.path.join('app', weights)

        config['weights'] = weights
        config['cnn']['pretrained'] = False
        config['device'] = 'cuda'
        config['predictor']['beamsearch'] = False

        return config
