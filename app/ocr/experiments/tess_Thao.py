
import pytesseract as pt
import cv2
import numpy as np

import time

from abc import ABC, abstractmethod

'''đối với window cần download Tesseract chọn bản tiếng việt
đối với Linux install tesseract, rồi download file vie.traineddata ở https://github.com/tesseract-ocr/tessdata,
Copy file vie.traineddata vào thư mục tessdata'''

''' input: image - cv
    output: string'''


# chỉ ra file tesseract ở đâu
pt.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-ocr\tesseract.exe'


class AbstractOCR(ABC):
    """
    ocr module base class
    """
    @abstractmethod
    def predict(self, image_object: np.ndarray) -> str:
        pass


class TesseractOCR(AbstractOCR):
    @staticmethod
    def preprocessing(img):
        # Rescale the image, if needed.
        img = cv2.resize(img, None, fx=1.5, fy=1.5, interpolation=cv2.INTER_CUBIC)
        # Converting to gray scale
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        # Apply dilation and erosion to remove some noise
        kernel = np.ones((1, 1), np.uint8)
        img = cv2.dilate(img, kernel, iterations=1)  # increases the white region in the image
        img = cv2.erode(img, kernel, iterations=1)  # erodes away the boundaries of foreground object
        
        # Apply threshold to get image with only b&w (binarization)
        img = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
    
        return img

    def get_text(self, image_object: np.ndarray) -> str:
        tic = time.time()
        # sau khi test xong thì thấy ko cần preprocessing thì nhận diện được chữ trắng =)))
        # img = self.preprocessing(img)
        result = pt.image_to_string(img, lang='vie')

        print(time.time() - tic)
        return result


if __name__ == "__main__":
    img = cv2.imread('C:/Users/Admin/Pictures/anh/text_trang.png')
    ocr = TesseractOCR()
    text = ocr.predict(img)
    print(text)
