import numpy as np

from interfaces.ocr_interfaces import AbstractOCR
from ocr.crnn.loader import CRnnLoader
from ocr.utils.image_processing import extract_line
from ocr.crnn.predict import predict
from ocr.config import CRnnConfig, Seq2SeqConfig

from vietocr.predict import Predictor
from PIL import Image


class CRnnOCR(AbstractOCR):
    """
    Wrapper for CRnn OCR Model.
    """

    def __init__(self):
        self.config = CRnnConfig()
        self.model = CRnnLoader.load(config=self.config)

    def preprocess(self, img: np.ndarray):
        """
        Some slight preprocessing for img taken from layout module.
        :param img: np.ndarray image
        :return: processed paragraph image
        """
        return img

    @staticmethod
    def extract_line(img):
        return extract_line(img)

    def get_text(self, img: np.ndarray, meta=None) -> str:
        """
        OCR text from input image
        :param img: input image as numpy array object of a line.
        :param meta: metadata for ocr extract such as: preprocessing...
        :return: text string
        """
        return predict(img, self.model, config=self.config,
                       imgW=None, imgH=32,
                       verbose=False, output_file_path=None)

    def get_text_batch(self, images: list) -> list:
        """
        Perform self.get_text on image batch.
        :param images: images iterator
        :param meta: metadata
        :return: list of string
        """
        return list(map(self.get_text, images))

    def get_text_para(self, img: np.ndarray, meta=None) -> list:
        """
        OCR text form input image as a paragraph image.
        :param img: input paragraph image
        :param meta: metadata for ocr extract such as: preprocessing...
        :return: list of text string
        """
        line_img_list = extract_line(img)
        return list(map(self.get_text, line_img_list))


class Seq2SeqOCR(AbstractOCR):
    """
    Wrapper for Sequence-to-Sequence OCR Model.
    """

    def __init__(self):
        self.config = Seq2SeqConfig.get_config()
        # self.model = CRnnLoader.load(config=self.config)
        print("Loading Seq2Seq Model")
        self.model = Predictor(config=self.config)

    def preprocess(self, img: np.ndarray):
        """
        Doing nothing hi hi.
        :param img: input image as numpy.ndarray
        :return: input image
        """
        return img

    def get_text(self, img: np.ndarray, meta=None):
        """
        Perform prediction using Seq2Seq model with weights from self.config
        :param img: input image as numpy.ndarray
        :param meta:
        :return:
        """
        img = Image.fromarray(np.uint8(img))
        s = self.model.predict(img)
        return s


class TesseractOCR(AbstractOCR):
    """
    Wrapper for Tesseract OCR Model.
    """

    def preprocess(self, img: np.ndarray):
        pass

    def get_text(self, img: np.ndarray, meta: dict) -> str:
        pass


class AttentionOCR(AbstractOCR):
    """
    Wrapper for Attention OCR Model.
    """
    def preprocess(self, img: np.ndarray):
        pass

    def get_text(self, img: np.ndarray, meta: dict):
        pass


class TransformerOCR(AbstractOCR):
    """
    Wrapper for Transformer-based OCR Model.
    """
    def preprocess(self, img: np.ndarray):
        pass

    def get_text(self, img: np.ndarray, meta: dict):
        pass


if __name__ == "__main__":
    import cv2
    ocr_engine = CRnnOCR()
    img = cv2.imread("samples/Capture.PNG")
    print(ocr_engine.get_text(img,meta={}))
