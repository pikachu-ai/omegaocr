import cv2
import numpy as np


class TextImageProcessing():
    def __init__(self):
        pass
    
    def denoise_binary_converter(self, image, blur = True):
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        if blur:
            gray = cv2.GaussianBlur(gray, (7,7), 0)
        # thresh = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]
        thresh = cv2.adaptiveThreshold(gray,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
                    cv2.THRESH_BINARY_INV,5,2)

        thresh = cv2.fastNlMeansDenoising(thresh, thresh, 50, 7, 21)
        _, thresh = cv2.threshold(thresh, 200, 255, cv2.THRESH_BINARY)

        return thresh
    
    @staticmethod
    def find_paragraph_contours(binary_image, return_dilate=False):
        # Create rectangular structuring element and dilate
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5,5))
        dilate = cv2.dilate(binary_image, kernel, iterations=4)

        # Find contours and draw rectangle
        cnts, hierarchy = cv2.findContours(dilate, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        
        if return_dilate:
            return cnts, hierarchy, dilate
        else:
            return cnts, hierarchy
        
    @staticmethod
    def sort_contours(cnts, method="top-to-bottom"):
        # initialize the reverse flag and sort index
        reverse = False
        i = 0
        # handle if we need to sort in reverse
        if method == "right-to-left" or method == "left-to-right":
            reverse = True
        # handle if we are sorting against the y-coordinate rather than
        # the x-coordinate of the bounding box
        if method == "top-to-bottom" or method == "bottom-to-top":
            i = 1
        # construct the list of bounding boxes and sort them from top to
        # bottom
        boundingBoxes = [cv2.boundingRect(c) for c in cnts]
        (cnts, boundingBoxes) = zip(*sorted(zip(cnts, boundingBoxes),
            key=lambda b:b[1][i], reverse=reverse))
        # return the list of sorted contours and bounding boxes
        return (cnts, boundingBoxes)
    
    @staticmethod
    def draw_contours_bounding_box(image, contours, color=(0,255,0)):
        for c in contours:
            x, y, w, h = cv2.boundingRect(c)
            cv2.rectangle(image, (x, y), (x + w, y + h), color, 2)
            
    @staticmethod
    def get_contour_rotated_box(contour):
        rbox = cv2.minAreaRect(contour)
        pts = cv2.boxPoints(rbox)
        pts = np.intp(pts)
        return pts
    
    @classmethod
    def draw_contour_rotated_box(cls, image, contour, color = (0,255,0)):
        """Draw the align-rotated bounding box for contour on image"""
        pts = cls.get_contour_rotated_box(contour)
        cv2.drawContours(image, [pts], -1, color, 1, cv2.LINE_AA)
        
    @staticmethod
    def order_points(pts):
        """return ordered coordinates of 4 points of a rectangle box in clock-wise, that:
        rect[0]: top-left
        rect[1]: top-right
        rect[2]: bottom-right
        rect[3]: bottom-left
        """
        rect = np.zeros((4, 2), dtype = "float32")
        # the top-left point will have the smallest sum, whereas
        # the bottom-right point will have the largest sum
        s = pts.sum(axis = 1)
        rect[0] = pts[np.argmin(s)]
        rect[2] = pts[np.argmax(s)]
        # now, compute the difference between the points, the
        # top-right point will have the smallest difference,
        # whereas the bottom-left will have the largest difference
        diff = np.diff(pts, axis = 1)
        rect[1] = pts[np.argmin(diff)]
        rect[3] = pts[np.argmax(diff)]
        # return the ordered coordinates
        return rect
    
    @staticmethod
    def paragraph_extracting(binary_image):
        """Extract paragraph regions from text image
        input: binary image
        output: list of paragraph region coordinates: [origin_x, origin_y, w, h]
        """
        paragraph_cnts, _ = TextImageProcessing.find_paragraph_contours(binary_image)
        paragraph_cnts, _ = TextImageProcessing.sort_contours(paragraph_cnts, method="top-to-bottom")

        para_roi_points = []
        for c in paragraph_cnts:    
            points = TextImageProcessing.get_contour_rotated_box(c)
            points = TextImageProcessing.order_points(points)
            para_roi_points.append(points) 

        return para_roi_points
    
    @staticmethod
    def object_extraction(sence_image, object_points):
        """Extract and align object in image
        Input: image contains object, 4 points of object conner
        Output: image of object only
        """

        pts1 = TextImageProcessing.order_points(object_points)
        h = int(np.linalg.norm(pts1[0]-pts1[3]))
        w = int(np.linalg.norm(pts1[0]-pts1[1]))

        pts2 = np.float32([[0,0],[w,0],[w,h],[0,h]])
        M = cv2.getPerspectiveTransform(pts1,pts2)
        dst = cv2.warpPerspective(sence_image, M, (w, h))

        return dst
    
    @staticmethod
    def line_segmentating(single_paragraph_image):
        """Extract line regions from single-paragraph image
        input: binary image
        output: list of line region coordinates: [origin_x, origin_y, w, h]
        """
        thresh = TextImageProcessing.denoise_binary_converter(single_paragraph_image)

        kernel = np.ones((1, 100), np.uint8)
        img_dilation = cv2.dilate(thresh, kernel, iterations=1)

        # find contours
        ctrs, hier = cv2.findContours(img_dilation.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        # sort contours
        # sorted_ctrs = sorted(ctrs, key=lambda ctr: cv2.boundingRect(ctr)[0])
        sorted_ctrs, _ = TextImageProcessing.sort_contours(ctrs, method="top-to-bottom")

        lines = [] # list of line: [[x, y, w, h]...]
        for i, ctr in enumerate(sorted_ctrs[:]):
            # Get bounding box
            x, y, w, h = cv2.boundingRect(ctr)
            lines.append([x, y, w, h])
            
        return lines
    
    @staticmethod
    def crop_to_contour_box(image, contour):
        """Crop the ractangle ROI of given contour"""
        x, y, w, h = cv2.boundingRect(contour)
        return image[y:y+h, x:x+w]
    
    @staticmethod
    def line_image_processing(line):
        """
        input: line (may be color) image
        output: normalized line binary image
        """
        threshed_line = TextImageProcessing.denoise_binary_converter(line, blur=False)
        line_cnts, _ = TextImageProcessing.find_paragraph_contours(threshed_line)
        # find the largest contour which should contain text region
        text_contour = max(line_cnts, key=cv2.contourArea)
        return TextImageProcessing.crop_to_contour_box(threshed_line, text_contour)

if __name__ == '__main__':
    pass
    