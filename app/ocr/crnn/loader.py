import torch
from ocr.crnn.crnn import CRNN
from interfaces.ocr_interfaces import ModelLoader
from ocr.config import CRnnConfig


class CRnnLoader(ModelLoader):
    """
    Static Class Loader for CRNN model.
    """

    @staticmethod
    def load(config: CRnnConfig):
        nc = 3
        imgH = 32
        model = CRNN(imgH, nc, config.nclass, 256)
        # if torch.cuda.is_available():
        #     model = model.cuda()
        print('loading CRNN-CTC model from %s' % config.model_path)
        model.load_state_dict(torch.load(config.model_path, map_location='cpu'))
        return model
