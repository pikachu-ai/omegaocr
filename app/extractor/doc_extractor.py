
from interfaces.doc_extraction_interfaces import AbstractDocExtraction
from .lib.image_processing import ImageProcessing

class DocExtractor(AbstractDocExtraction):

    def __init__(self):
        self.extractor = ImageProcessing()

    def get_doc_image(self, image, debug_dir=None):
        return self.extractor.get_object(image, debug_dir)

