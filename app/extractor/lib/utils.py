import cv2
import numpy as np

def erode_orientation(threshold, iterations=1, is_vertical=False):
    
    kernel = np.ones((1, 3), dtype="uint8")
    if is_vertical:
        kernel = np.ones((3, 1), dtype="uint8")

    left = cv2.erode(threshold.copy(), kernel, iterations=iterations, anchor=(0,0))
    right = cv2.erode(threshold.copy(), kernel, iterations=iterations, anchor=(kernel.shape[1]-1,kernel.shape[0]-1))
    
    bitwise = cv2.bitwise_and(left, right)

    return bitwise


if __name__ == "__main__":
    image = np.ones((300, 300), dtype=np.uint8)*255
    image[[0, 299], :] = 0
    image[:, [0, 299]] = 0
    thresh = erode_orientation(image, iterations=10, is_vertical=True)
    