import os
import cv2
import numpy as np
import random as rng
import imutils
from .utils import erode_orientation

# cv2.namedWindow('', cv2.WINDOW_NORMAL)

class ImageProcessing:
    def __init__(self):
        pass
    
    def threshold_image(self, image, debug_dir=None):
        thresh = cv2.Canny(image,1,100)
        if debug_dir:
            cv2.imwrite(os.path.join(debug_dir, 'Canny.jpg'), thresh)
        kernel = np.ones((3,3), dtype=np.uint8)
        thresh = 255-cv2.dilate(thresh, kernel, iterations=10)
        thresh = erode_orientation(thresh, iterations=1)
        thresh = erode_orientation(thresh, iterations=1, is_vertical=True)

        if debug_dir:
            cv2.imwrite(os.path.join(debug_dir, 'initial_threshold.jpg'), thresh)
        return thresh

    def split_main_object(self, thresh, debug_dir=None):
        img_h, img_w = thresh.shape
        
        kernel = np.ones((3,3), dtype=np.uint8)
        num_labels, labels, stats, centroids = cv2.connectedComponentsWithStats(thresh , 4 , cv2.CV_32S)
        max_idx = -1
        c_h = -1
        for idx, stat in enumerate(stats):
            x, y, w, h, c = stat

            if w > img_w-10 or h > img_h-10:
                thresh[labels==idx] = 0
                continue
            if w > c_h and np.sum(thresh[labels==idx]) >0:
                c_h = h
                max_idx = idx
        
        if max_idx == -1:
            return None, None
        mx, my, mw, mh, _ = stats[max_idx]
        mcx = mx+mw/2
        mcy = my+mh/2
        thresh = np.where(labels==max_idx, 255, 0).astype(np.uint8)

        if debug_dir:
            cv2.imwrite(os.path.join(debug_dir, 'main_threshold.jpg'), thresh)
        
        return thresh, (mx-20, my-20, mw+40, mh+40)
    
    def highlight_object(self, thresh, debug_dir=None):
        kernel = np.ones((3,3), dtype=np.uint8)
        thresh = cv2.dilate(thresh, kernel, iterations=20)
        contours, hierarchy = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        
        drawing = np.zeros(thresh.shape, dtype=np.uint8)
        h, w = thresh.shape
        debug = np.zeros((h, w, 3), dtype=np.uint8)

        for i in range(len(contours)):
            
            cnt = contours[i]
            area = cv2.contourArea(cnt)
            if area < 50000:
                continue
            epsilon = 0.05*cv2.arcLength(cnt,True)
            approx = cv2.approxPolyDP(cnt,epsilon,True)
            color = 255
            # cv2.drawContours(drawing, [cnt], 0, color, 5, cv2.LINE_8, hierarchy, 0)
            # cv2.drawContours(drawing,[cnt], 0, color, -1)
            cv2.drawContours(drawing,[cnt], 0, color, 1)
            cv2.drawContours(debug,[cnt], 0, (0, 255, 0), 5)
        
        if debug_dir:
            
            cv2.imwrite(os.path.join(debug_dir, 'highlight_object.jpg'), debug)
        return drawing
    
    # def get_corresponding_lines()
    
    def get_insight_coord(self, thresh, bbox, debug_dir=None):
        thresh = thresh.copy() # đường biên của vật thể

        gx, gy, gw, gh = bbox

        v_sum = np.sum(thresh/255, axis=0)
        h_sum = np.sum(thresh/255, axis=1)
        
        h_sum = np.where(h_sum==2, 1, 0).reshape((1,-1)).astype(np.uint8)
        v_sum = np.where(v_sum==2, 1, 0).reshape((1,-1)).astype(np.uint8)
        h_sum = erode_orientation(h_sum, iterations=10).ravel()
        v_sum = erode_orientation(v_sum, iterations=10).ravel()
        v_args = np.argwhere(h_sum!=0)
        h_args = np.argwhere(v_sum!=0)

        if len(h_args) == 0 or len(v_args) == 0:
            return None
        min_x = np.min(h_args)
        max_x = np.max(h_args)
        min_y = np.min(v_args)
        max_y = np.max(v_args)

        if debug_dir:
            debug = np.zeros((thresh.shape[0], thresh.shape[1], 3), dtype=np.uint8)
            debug[thresh != 0] = 255
            cv2.rectangle(debug, (min_x, gy), (max_x, min_y), (255, 0, 0), 5)
            cv2.rectangle(debug, (max_x, min_y),(gx+gw, max_y) , (0, 255, 0), 5)
            cv2.rectangle(debug, (min_x, max_y), (max_x, gy+gh), (255, 0, 0), 5)
            cv2.rectangle(debug, (gx, min_y), (min_x, max_y), (0, 0, 255), 5)

            cv2.imwrite(os.path.join(debug_dir, 'limited_thresh.jpg'), debug)
        
        return (min_x, min_y, max_x, max_y)
    
    def get_line_from_thresh_area(self, thresh, base=(0,0)):
        by, bx = base
        points = np.argwhere(thresh != 0)
        
        xs = points[:, 1] + bx
        ys = points[:, 0] + by
        m, b = np.polyfit(xs, ys, 1)
        
        return (m, b)
    
    def find_intersection(self, p1, p2):
        m1, b1 = p1
        m2, b2 = p2
        x = (b1-b2)/(m2-m1)
        y = b1 + m1*x

        return np.array((int(x), int(y)))
    
    def find_lines(self, threshold, outsize_box, insight_box):
        threshold = threshold.copy()
        ix1, iy1, ix2, iy2 = insight_box
        ox1, oy1, ow, oh = outsize_box
        ox2 = ox1 + ow
        oy2 = oy1 + oh
        delta = 8

        p1 = self.get_line_from_thresh_area(threshold[oy1:iy1, ix1: ix2], (oy1+delta, ix1))
        p2 = self.get_line_from_thresh_area(threshold[iy1:iy2, ix2: ox2], (iy1, ix2-delta))
        p3 = self.get_line_from_thresh_area(threshold[iy2:oy2, ix1: ix2], (iy2-delta, ix1))
        p4 = self.get_line_from_thresh_area(threshold[iy1:iy2, ox1: ix1], (iy1, ox1+delta))

        return p1, p2, p3, p4
    
    def get_coordinates(self, lines, shape, debug_dir=None):
        p1, p2, p3, p4 = lines

        x1, y1 = self.find_intersection(p4, p1)
        x2, y2 = self.find_intersection(p1, p2)
        x3, y3 = self.find_intersection(p2, p3)
        x4, y4 = self.find_intersection(p3, p4)
        
        if debug_dir:
            h, w = shape
            debug = np.zeros((h, w, 3), dtype='uint8')
            cv2.line(debug, (x1, y1), (x2, y2), (255, 0, 0), 5)
            cv2.line(debug, (x2, y2), (x3, y3), (0, 255, 0), 5)
            cv2.line(debug, (x3, y3), (x4, y4), (255, 0, 0), 5)
            cv2.line(debug, (x4, y4), (x1, y1), (0, 255, 0), 5)

            cv2.imwrite(os.path.join(debug_dir, 'final_line.jpg'), debug)
        
        return np.array([(x1, y1), (x2, y2), (x3, y3), (x4, y4)])
    
    def four_point_transform(self, image, rect, debug_dir=None):
        rect = rect.astype(np.float32)
        (tl, tr, br, bl) = rect
        widthA = np.linalg.norm(tl - tr)
        widthB = np.linalg.norm(bl - br)
        maxWidth = max(int(widthA), int(widthB))
        heightA = np.linalg.norm(tr - br)
        heightB = np.linalg.norm(tl - bl)
        maxHeight = max(int(heightA), int(heightB))
        dst = np.array([
            [0, 0],
            [maxWidth - 1, 0],
            [maxWidth - 1, maxHeight - 1],
            [0, maxHeight - 1]], dtype = "float32")

        M = cv2.getPerspectiveTransform(rect, dst)
        warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))

        if debug_dir:
            cv2.imwrite(os.path.join(debug_dir, 'document.jpg'), warped)
        return warped

    def get_object(self, image, debug_dir=None):
        original_shape = image.shape
        original_image = image.copy()
        image = imutils.resize(image, width=min(1000, original_shape[1]))

        thresh = self.threshold_image(image, debug_dir)
        thresh, bbox = self.split_main_object(thresh, debug_dir)
        thresh = self.highlight_object(thresh, debug_dir)
        insight_box = self.get_insight_coord(thresh, bbox, debug_dir)
        if insight_box is None:
            return original_image
        lines = self.find_lines(thresh, bbox, insight_box)

        coords = self.get_coordinates(lines, thresh.shape, debug_dir)

        # get target image with original scale
        old_shape = np.array(original_shape[:2])
        original_coords = ((old_shape/thresh.shape)[::-1] * coords).astype(int)
        document_image = self.four_point_transform(original_image, original_coords, debug_dir)
        
        if debug_dir:
            mask = np.zeros(old_shape, dtype=np.uint8)
            cv2.drawContours(mask,[np.array(original_coords).reshape(-1, 1, 2)], 0, 255, -1)
            main_obj = np.zeros(original_image.shape, dtype=np.uint8)
            main_obj[mask!=0] = original_image[mask!=0]

            cv2.imwrite(os.path.join(debug_dir, 'original.jpg'), image)
            cv2.imwrite(os.path.join(debug_dir, 'target_doc.jpg'), main_obj)
        
        h, w, _ = document_image.shape

        return document_image



    def analyze_line(self, image, debug_dir=None):
        pass

    def get_layout(self, image, debug_dir=None):
        image = image.copy()
        main_img = self.get_object(image, debug_dir)
        line_data = self.analyze_line(main_img, debug_dir)
        return main_img     