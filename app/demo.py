import base64
import numpy as np
import cv2
import json
import sys
import os

from controller.controller import VanillaController
from detection.detect import RCNNDetector
from ocr.ocr import CRnnOCR, Seq2SeqOCR
from layout_analysis.layout import RuleBasedLayout
from reconstruct.reconstruct import DocxReconstructor
from extractor.doc_extractor import DocExtractor
from text_correct.text_correct import NgramCorrection


if __name__ == "__main__":
    # import json
    # img_path1 = "detection/images/test.jpg"
    # image = cv2.imread(img_path1)
    # detector = Layout()
    # results = detector.get_layout(image)
    # for x,y,w,h, l, s in results:
    #     cv2.rectangle(image, (x, y), (x+w, y+h), (0,255, 0), 4)
    # cv2.namedWindow('', cv2.WINDOW_NORMAL)
    # cv2.imshow('', image)
    # cv2.waitKey(0)

    # import cv2
    # config = CRnnConfig()
    # ocr_engine = CRnnOCR(config=config)
    # img = cv2.imread("ocr/samples/Capture.PNG")
    # cv2.imshow("input image", img)
    # print(ocr_engine.get_text(img, meta={}))
    # cv2.waitKey()

    detect_engine = RCNNDetector()
    # ocr_engine = CRnnOCR()
    # ocr_engine = Seq2SeqOCR()
    # correct_engine = NgramCorrection()
    # layout_engine = RuleBasedLayout()
    reconstructor = DocxReconstructor()
    extractor = DocExtractor()

    detector = VanillaController(detect_engine=detect_engine,
                                 ocr_engine=ocr_engine,
                                 correct_engine=correct_engine,
                                 layout_engine=layout_engine,
                                 reconstructor=reconstructor,
                                 extractor=extractor)
    img_path1 = "../cameraImg/6.jpg"
    image = cv2.imread(img_path1)
    results = detector.gen_docx(image)
    os.system('start save.docx')
